<?php
if (!function_exists('pabloguadi_theme_shortcodes_setup')) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_theme_shortcodes_setup', 1 );
	function pabloguadi_theme_shortcodes_setup() {
		add_filter('pabloguadi_filter_googlemap_styles', 'pabloguadi_theme_shortcodes_googlemap_styles');
	}
}


// Add theme-specific Google map styles
if ( !function_exists( 'pabloguadi_theme_shortcodes_googlemap_styles' ) ) {
	function pabloguadi_theme_shortcodes_googlemap_styles($list) {
		$list['simple']		= esc_html__('Simple', 'pabloguadi');
		$list['greyscale']	= esc_html__('Greyscale', 'pabloguadi');
		$list['inverse']	= esc_html__('Inverse', 'pabloguadi');
		$list['apple']		= esc_html__('Apple', 'pabloguadi');
		return $list;
	}
}
?>