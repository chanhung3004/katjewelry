<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'pabloguadi_template_header_8_theme_setup' ) ) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_template_header_8_theme_setup', 1 );
	function pabloguadi_template_header_8_theme_setup() {
		pabloguadi_add_template(array(
			'layout' => 'header_8',
			'mode'   => 'header',
			'title'  => esc_html__('Header 8', 'pabloguadi'),
			'icon'   => pabloguadi_get_file_url('templates/headers/images/8.jpg')
			));
	}
}

// Template output
if ( !function_exists( 'pabloguadi_template_header_8_output' ) ) {
	function pabloguadi_template_header_8_output($post_options, $post_data) {

        // Top of page section: page title and breadcrumbs
        $show_title = pabloguadi_get_custom_option('show_page_title')=='yes';
        $show_navi = apply_filters('pabloguadi_filter_show_post_navi', false);
        $show_breadcrumbs = pabloguadi_get_custom_option('show_breadcrumbs')=='yes';

		// WP custom header
		$header_css = '';
		if ($post_options['position'] != 'over') {
			$header_image = get_header_image();
			$header_css = $header_image!='' 
				? ' style="background-image: url('.esc_url($header_image).')"' 
				: '';
		}

        if (empty($header_image))
            $header_image = pabloguadi_get_custom_option('top_panel_image');
        if (empty($header_image))
            $header_image = get_header_image();
        if (!empty($header_image)) {
            $header_css = ' style="background-image: url('.esc_url($header_image).')"';
        }
		?>

		<div class="top_panel_fixed_wrap"></div>

		<header class="top_panel_wrap top_panel_style_8 scheme_<?php echo esc_attr($post_options['scheme']); ?> ">
			
			<div class="top_panel_wrap_inner top_panel_inner_style_8 top_panel_position_<?php echo esc_attr(pabloguadi_get_custom_option('top_panel_position')); ?> " <?php pabloguadi_show_layout($header_css); ?>>
			
				<?php if (pabloguadi_get_custom_option('show_top_panel_top')=='yes') { ?>
				<div class="top_panel_top">
					<div class="content_wrap clearfix">
						<?php
						pabloguadi_template_set_args('top-panel-top', array(
							'top_panel_top_components' => array('contact_info', 'login', 'currency', 'bookmarks', 'socials')
						));
						get_template_part(pabloguadi_get_file_slug('templates/headers/_parts/top-panel-top.php'));
						?>
					</div>
				</div>
				<?php } ?>

				<div class="top_panel_middle <?php echo ((!is_front_page() && ($show_title || $show_breadcrumbs)) ? '' : esc_attr('without_top_panel_title')) ?>">
					<div class="content_wrap">
                        <div class="columns_wrap columns_fluid"><?php
                            // Phone and email
                            $contact_phone=trim(pabloguadi_get_custom_option('contact_phone'));
                            $contact_email=trim(pabloguadi_get_custom_option('contact_email'));
                            if (!empty($contact_phone) || !empty($contact_email)) {
                                ?><div class="column-1_4 contact_field contact_phone">
                                <span class="contact_phone"><?php echo wp_kses_data(force_balance_tags($contact_phone)); ?></span>
                                <span class="contact_email"><?php echo wp_kses_data(force_balance_tags($contact_email)); ?></span>
                                </div><?php
                            }
                            ?><div class="column-1_2 contact_logo">
                                <?php pabloguadi_show_logo(); ?>
                            </div><div class="column-1_4 contact_field">
                                <div class="menu_pushy_wrap clearfix">
                                    <a href="#" class="menu_pushy_button icon-menu-1"></a>
                                </div>
                                <?php
                                // Woocommerce Cart
                                if (function_exists('pabloguadi_exists_woocommerce') && pabloguadi_exists_woocommerce() && (pabloguadi_is_woocommerce_page() && pabloguadi_get_custom_option('show_cart')=='shop' || pabloguadi_get_custom_option('show_cart')=='always') && !(is_checkout() || is_cart() || defined('WOOCOMMERCE_CHECKOUT') || defined('WOOCOMMERCE_CART'))) {
                                    ?><div class="contact_cart"><?php get_template_part(pabloguadi_get_file_slug('templates/headers/_parts/contact-info-cart.php')); ?></div><?php
                                }
                                ?>
                            </div>

                            </div>
					</div>
				</div>

                <?php
                if (!is_front_page() && ($show_title || $show_breadcrumbs)) {
                    ?>
                    <div class="top_panel_title">
                        <div class="top_panel_title_inner">
                            <div class="content_wrap">
                                <?php
                                if ($show_title) {
                                    if ($show_navi) {
                                        ?><div class="post_navi"><?php
                                        previous_post_link( '<span class="post_navi_item post_navi_prev">%link</span>', '%title', true, '', 'product_cat' );
                                        next_post_link( '<span class="post_navi_item post_navi_next">%link</span>', '%title', true, '', 'product_cat' );
                                        ?></div><?php
                                    } else {
                                        ?><h1 class="page_title"><?php echo strip_tags(pabloguadi_get_blog_title()); ?></h1><?php
                                    }
                                }
                                if ($show_breadcrumbs) {
                                    ?><div class="breadcrumbs"><?php if (!is_404()) pabloguadi_show_breadcrumbs(); ?></div><?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>

			</div>

		</header>

		<nav class="menu_pushy_nav_area pushy pushy-right">
			<div class="pushy_inner">
                <a href="#" class="close-pushy"></a>
                <div class="menu_pushy_nav_header"><?php echo esc_html('Menu', 'pabloguadi'); ?></div>
	
				<?php 

				$menu_main = pabloguadi_get_nav_menu('menu_main');
				if (empty($menu_main)) $menu_main = pabloguadi_get_nav_menu();
				echo str_replace('menu_main', 'menu_pushy', $menu_main);
	
				$address_1 = pabloguadi_get_theme_option('contact_address_1');
				$address_2 = pabloguadi_get_theme_option('contact_address_2');
				$phone = pabloguadi_get_theme_option('contact_phone');
				$fax = pabloguadi_get_theme_option('contact_fax');

				if (!empty($address_1) || !empty($address_2) || !empty($phone) || !empty($contact_email)) {
					?>
                    <div class="menu_pushy_nav_header"><?php echo esc_html('Contact me', 'pabloguadi'); ?></div>
					<div class="contact_info">
						<?php if (!empty($address_1) || !empty($address_2)) { ?>
							<address class="contact_address">
								<?php pabloguadi_show_layout(($address_1) . (!empty($address_1) ? ', ' : '') . trim($address_2)); ?>
							</address>
						<?php } ?>
						<?php if (!empty($phone) || !empty($fax)) { ?>
							<address class="contact_phones">
								<?php echo($phone); ?>
							</address>
                            <address class="contact_phones">
                                <?php echo ($contact_email); ?>
                            </address>
						<?php } ?>
					</div>
					<?php
				}
	
				if (pabloguadi_get_custom_option('show_socials')=='yes') {
					?>
					<div class="contact_socials">
						<?php pabloguadi_show_layout(pabloguadi_sc_socials(array('size'=>'tiny'))); ?>
					</div>
					<?php
				}
				?>

			</div>
        </nav>

        <!-- Site Overlay -->
        <div class="site-overlay"></div>
		<?php
		pabloguadi_storage_set('header_mobile', array(
				 'open_hours' => false,
				 'login' => false,
				 'socials' => false,
				 'bookmarks' => false,
				 'contact_address' => false,
				 'contact_phone_email' => false,
				 'woo_cart' => false,
				 'search' => false
			)
		);
	}
}
?>