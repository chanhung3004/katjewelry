<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'pabloguadi_template_header_5_theme_setup' ) ) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_template_header_5_theme_setup', 1 );
	function pabloguadi_template_header_5_theme_setup() {
		pabloguadi_add_template(array(
			'layout' => 'header_5',
			'mode'   => 'header',
			'title'  => esc_html__('Header 5', 'pabloguadi'),
			'icon'   => pabloguadi_get_file_url('templates/headers/images/5.jpg')
			));
	}
}

// Template output
if ( !function_exists( 'pabloguadi_template_header_5_output' ) ) {
	function pabloguadi_template_header_5_output($post_options, $post_data) {

		// WP custom header
		$header_css = '';
		if ($post_options['position'] != 'over') {
			$header_image = get_header_image();
			$header_css = $header_image!='' 
				? ' style="background-image: url('.esc_url($header_image).')"' 
				: '';
		}
		?>

		<div class="top_panel_fixed_wrap"></div>

		<header class="top_panel_wrap top_panel_style_5 scheme_<?php echo esc_attr($post_options['scheme']); ?>">
			<div class="top_panel_wrap_inner top_panel_inner_style_5 top_panel_position_<?php echo esc_attr(pabloguadi_get_custom_option('top_panel_position')); ?>">
			
			<?php if (pabloguadi_get_custom_option('show_top_panel_top')=='yes') { ?>
				<div class="top_panel_top">
					<div class="content_wrap clearfix">
						<?php
						pabloguadi_template_set_args('top-panel-top', array(
							'top_panel_top_components' => array('contact_info', 'login', 'cart')
						));
						get_template_part(pabloguadi_get_file_slug('templates/headers/_parts/top-panel-top.php'));
						?>
					</div>
				</div>
			<?php } ?>

			<div class="top_panel_middle" <?php pabloguadi_show_layout($header_css); ?>>
                <div class="contact_logo">
                    <div class="content_wrap">
                        <?php pabloguadi_show_logo(true, true); ?>
                    </div>

                </div>
                <div class="menu_main_wrap clearfix">
                    <div class="content_wrap">
                        <nav class="menu_main_nav_area menu_hover_<?php echo esc_attr(pabloguadi_get_theme_option('menu_hover')); ?>">
                            <?php
                            $menu_main = pabloguadi_get_nav_menu('menu_main');
                            if (empty($menu_main)) $menu_main = pabloguadi_get_nav_menu();
							pabloguadi_show_layout($menu_main);
                            ?>
                        </nav></div>
                </div>
			</div>

			</div>
		</header>
        <?php
        // Top of page section: page title and breadcrumbs
        $show_title = pabloguadi_get_custom_option('show_page_title')=='yes';
        $show_navi = apply_filters('pabloguadi_filter_show_post_navi', false);
        $show_breadcrumbs = pabloguadi_get_custom_option('show_breadcrumbs')=='yes';
        if ($show_title || $show_breadcrumbs) {
            ?>
            <div class="top_panel_title">
                <div class="top_panel_title_inner">
                    <div class="content_wrap">
                        <?php
                        if ($show_title) {
                            if ($show_navi) {
                                ?><div class="post_navi"><?php
                                previous_post_link( '<span class="post_navi_item post_navi_prev">%link</span>', '%title', true, '', 'product_cat' );
                                next_post_link( '<span class="post_navi_item post_navi_next">%link</span>', '%title', true, '', 'product_cat' );
                                ?></div><?php
                            } else {
                                ?><h1 class="page_title"><?php echo strip_tags(pabloguadi_get_blog_title()); ?></h1><?php
                            }
                        }
                        if ($show_breadcrumbs) {
                            ?><div class="breadcrumbs"><?php if (!is_404()) pabloguadi_show_breadcrumbs(); ?></div><?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>

		<?php
		pabloguadi_storage_set('header_mobile', array(
				 'open_hours' => false,
				 'login' => false,
				 'socials' => false,
				 'bookmarks' => false,
				 'contact_address' => false,
				 'contact_phone_email' => false,
				 'woo_cart' => false,
				 'search' => false
			)
		);
	}
}
?>