<?php 
// Get template args
extract(pabloguadi_template_get_args('top-panel-top'));

if (in_array('contact_info', $top_panel_top_components)) {
    $contact_phone=trim(pabloguadi_get_custom_option('contact_phone'));
    $contact_email=trim(pabloguadi_get_custom_option('contact_email'));
    if (!empty($contact_phone) || !empty($contact_email)) {
        ?>
        <div class="top_panel_top_contact_area contact_field ">
            <span class="contact_phone"><?php echo wp_kses_data(force_balance_tags($contact_phone)); ?></span>
            <span class="contact_email"><?php echo wp_kses_data(force_balance_tags($contact_email)); ?></span>
        </div>
    <?php
    }
}
?>

<?php
if (in_array('open_hours', $top_panel_top_components) && ($open_hours=trim(pabloguadi_get_custom_option('contact_open_hours')))!='') {
	?>
	<div class="top_panel_top_open_hours icon-clock"><?php echo wp_kses_data(force_balance_tags($open_hours)); ?></div>
	<?php
}
?>

<div class="top_panel_top_user_area contact_field ">
	<?php
	if (in_array('socials', $top_panel_top_components) && pabloguadi_get_custom_option('show_socials')=='yes') {
		?>
		<div class="top_panel_top_socials">
			<?php pabloguadi_show_layout(pabloguadi_sc_socials(array('size'=>'tiny'))); ?>
		</div>
		<?php
	}

	if (in_array('search', $top_panel_top_components) && pabloguadi_get_custom_option('show_search')=='yes') {
		?>
		<div class="top_panel_top_search"><?php pabloguadi_show_layout(pabloguadi_sc_search(array('state' => pabloguadi_get_theme_option('search_style')=='default' ? 'fixed' : 'closed'))); ?></div>
		<?php
	}

	$menu_user = pabloguadi_get_nav_menu('menu_user');
	if (empty($menu_user)) {
		?>
		<ul id="<?php echo (!empty($menu_user_id) ? esc_attr($menu_user_id) : 'menu_user'); ?>" class="menu_user_nav">
		<?php
	} else {
		$menu = pabloguadi_substr($menu_user, 0, pabloguadi_strlen($menu_user)-5);
		$pos = pabloguadi_strpos($menu, '<ul');
		if ($pos!==false && pabloguadi_strpos($menu, 'menu_user_nav')===false)
			$menu = pabloguadi_substr($menu, 0, $pos+3) . ' class="menu_user_nav"' . pabloguadi_substr($menu, $pos+3);
		if (!empty($menu_user_id))
			$menu = pabloguadi_set_tag_attrib($menu, '<ul>', 'id', $menu_user_id);
		echo str_replace('class=""', '', $menu);
	}
	

	if (in_array('currency', $top_panel_top_components) && function_exists('pabloguadi_is_woocommerce_page') && pabloguadi_is_woocommerce_page() && pabloguadi_get_custom_option('show_currency')=='yes') {
		?>
		<li class="menu_user_currency">
			<a href="#">$</a>
			<ul>
				<li><a href="#"><b>&#36;</b> <?php esc_html_e('Dollar', 'pabloguadi'); ?></a></li>
				<li><a href="#"><b>&euro;</b> <?php esc_html_e('Euro', 'pabloguadi'); ?></a></li>
				<li><a href="#"><b>&pound;</b> <?php esc_html_e('Pounds', 'pabloguadi'); ?></a></li>
			</ul>
		</li>
		<?php
	}

	if (in_array('language', $top_panel_top_components) && pabloguadi_get_custom_option('show_languages')=='yes' && function_exists('icl_get_languages')) {
		$languages = icl_get_languages('skip_missing=1');
		if (!empty($languages) && is_array($languages)) {
			$lang_list = '';
			$lang_active = '';
			foreach ($languages as $lang) {
				$lang_title = esc_attr($lang['translated_name']);	//esc_attr($lang['native_name']);
				if ($lang['active']) {
					$lang_active = $lang_title;
				}
				$lang_list .= "\n"
					.'<li><a rel="alternate" hreflang="' . esc_attr($lang['language_code']) . '" href="' . esc_url(apply_filters('WPML_filter_link', $lang['url'], $lang)) . '">'
						.'<img src="' . esc_url($lang['country_flag_url']) . '" alt="' . esc_attr($lang_title) . '" title="' . esc_attr($lang_title) . '" />'
						. ($lang_title)
					.'</a></li>';
			}
			?>
			<li class="menu_user_language">
				<a href="#"><span><?php pabloguadi_show_layout($lang_active); ?></span></a>
				<ul><?php pabloguadi_show_layout($lang_list); ?></ul>
			</li>
			<?php
		}
	}

	if (in_array('bookmarks', $top_panel_top_components) && pabloguadi_get_custom_option('show_bookmarks')=='yes') {
		// Load core messages
		pabloguadi_enqueue_messages();
		?>
		<li class="menu_user_bookmarks"><a href="#" class="bookmarks_show icon-star" title="<?php esc_attr_e('Show bookmarks', 'pabloguadi'); ?>"><?php esc_html_e('Bookmarks', 'pabloguadi'); ?></a>
		<?php 
			$list = pabloguadi_get_value_gpc('pabloguadi_bookmarks', '');
			if (!empty($list)) $list = json_decode($list, true);
			?>
			<ul class="bookmarks_list">
				<li><a href="#" class="bookmarks_add icon-star-empty" title="<?php esc_attr_e('Add the current page into bookmarks', 'pabloguadi'); ?>"><?php esc_html_e('Add bookmark', 'pabloguadi'); ?></a></li>
				<?php 
				if (!empty($list) && is_array($list)) {
					foreach ($list as $bm) {
						echo '<li><a href="'.esc_url($bm['url']).'" class="bookmarks_item">'.($bm['title']).'<span class="bookmarks_delete icon-cancel" title="'.esc_attr__('Delete this bookmark', 'pabloguadi').'"></span></a></li>';
					}
				}
				?>
			</ul>
		</li>
		<?php 
	}

	if (in_array('login', $top_panel_top_components) && pabloguadi_get_custom_option('show_login')=='yes') {
		if ( !is_user_logged_in() ) {
			// Load core messages
			pabloguadi_enqueue_messages();
			// Anyone can register ?
			if ( (int) get_option('users_can_register') > 0) {
				?><li class="menu_user_register"><a href="#popup_registration" class="popup_link popup_register_link icon-pencil"><?php esc_html_e('Register', 'pabloguadi'); ?></a></li><?php
			}
			?><li class="menu_user_login"><a href="#popup_login" class="popup_link popup_login_link icon-user"><?php esc_html_e('Login', 'pabloguadi'); ?></a></li><?php 
		} else {
			$current_user = wp_get_current_user();
			?>
			<li class="menu_user_controls">
				<a href="#"><?php
					$user_avatar = '';
					$mult = pabloguadi_get_retina_multiplier();
					if ($current_user->user_email) $user_avatar = get_avatar($current_user->user_email, 16*$mult);
					if ($user_avatar) {
						?><span class="user_avatar"><?php pabloguadi_show_layout($user_avatar); ?></span><?php
					}?><span class="user_name"><?php pabloguadi_show_layout($current_user->display_name); ?></span></a>
				<ul>
					<?php if (current_user_can('publish_posts')) { ?>
					<li><a href="<?php echo esc_url(home_url('/')); ?>/wp-admin/post-new.php?post_type=post" class="icon icon-doc"><?php esc_html_e('New post', 'pabloguadi'); ?></a></li>
					<?php } ?>
					<li><a href="<?php echo get_edit_user_link(); ?>" class="icon icon-cog"><?php esc_html_e('Settings', 'pabloguadi'); ?></a></li>
				</ul>
			</li>
			<li class="menu_user_logout"><a href="<?php echo esc_url(wp_logout_url(home_url('/'))); ?>" class="icon icon-logout"><?php esc_html_e('Logout', 'pabloguadi'); ?></a></li>
			<?php 
		}
	}

	if (in_array('cart', $top_panel_top_components) && function_exists('pabloguadi_exists_woocommerce') && pabloguadi_exists_woocommerce() && (pabloguadi_is_woocommerce_page() && pabloguadi_get_custom_option('show_cart')=='shop' || pabloguadi_get_custom_option('show_cart')=='always') && !(is_checkout() || is_cart() || defined('WOOCOMMERCE_CHECKOUT') || defined('WOOCOMMERCE_CART'))) { 
		?>
		<li class="menu_user_cart contact_cart">
			<?php get_template_part(pabloguadi_get_file_slug('templates/headers/_parts/contact-info-cart.php')); ?>
		</li>
		<?php
	}
	?>

	</ul>

</div>