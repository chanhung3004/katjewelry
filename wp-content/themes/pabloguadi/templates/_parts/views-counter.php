<?php 
if (is_singular()) {
	if (pabloguadi_get_theme_option('use_ajax_views_counter')=='yes') {
		pabloguadi_storage_set_array('js_vars', 'ajax_views_counter', array(
			'post_id' => get_the_ID(),
			'post_views' => pabloguadi_get_post_views(get_the_ID())
		));
	} else
		pabloguadi_set_post_views(get_the_ID());
}
?>