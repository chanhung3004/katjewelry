<?php
/**
 * PabloGuadi Framework: Theme options custom fields
 *
 * @package	pabloguadi
 * @since	pabloguadi 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'pabloguadi_options_custom_theme_setup' ) ) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_options_custom_theme_setup' );
	function pabloguadi_options_custom_theme_setup() {

		if ( is_admin() ) {
			add_action("admin_enqueue_scripts",	'pabloguadi_options_custom_load_scripts');
		}
		
	}
}

// Load required styles and scripts for custom options fields
if ( !function_exists( 'pabloguadi_options_custom_load_scripts' ) ) {
	//add_action("admin_enqueue_scripts", 'pabloguadi_options_custom_load_scripts');
	function pabloguadi_options_custom_load_scripts() {
		pabloguadi_enqueue_script( 'pabloguadi-options-custom-script',	pabloguadi_get_file_url('core/core.options/js/core.options-custom.js'), array(), null, true );	
	}
}


// Show theme specific fields in Post (and Page) options
if ( !function_exists( 'pabloguadi_show_custom_field' ) ) {
	function pabloguadi_show_custom_field($id, $field, $value) {
		$output = '';
		switch ($field['type']) {
			case 'reviews':
				$output .= '<div class="reviews_block">' . trim(pabloguadi_reviews_get_markup($field, $value, true)) . '</div>';
				break;
	
			case 'mediamanager':
				wp_enqueue_media( );
				$output .= '<a id="'.esc_attr($id).'" class="button mediamanager pabloguadi_media_selector"
					data-param="' . esc_attr($id) . '"
					data-choose="'.esc_attr(isset($field['multiple']) && $field['multiple'] ? esc_html__( 'Choose Images', 'pabloguadi') : esc_html__( 'Choose Image', 'pabloguadi')).'"
					data-update="'.esc_attr(isset($field['multiple']) && $field['multiple'] ? esc_html__( 'Add to Gallery', 'pabloguadi') : esc_html__( 'Choose Image', 'pabloguadi')).'"
					data-multiple="'.esc_attr(isset($field['multiple']) && $field['multiple'] ? 'true' : 'false').'"
					data-linked-field="'.esc_attr($field['media_field_id']).'"
					>' . (isset($field['multiple']) && $field['multiple'] ? esc_html__( 'Choose Images', 'pabloguadi') : esc_html__( 'Choose Image', 'pabloguadi')) . '</a>';
				break;
		}
		return apply_filters('pabloguadi_filter_show_custom_field', $output, $id, $field, $value);
	}
}
?>