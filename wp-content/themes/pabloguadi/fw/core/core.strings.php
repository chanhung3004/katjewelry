<?php
/**
 * PabloGuadi Framework: strings manipulations
 *
 * @package	pabloguadi
 * @since	pabloguadi 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Check multibyte functions
if ( ! defined( 'PABLOGUADI_MULTIBYTE' ) ) define( 'PABLOGUADI_MULTIBYTE', function_exists('mb_strpos') ? 'UTF-8' : false );

if (!function_exists('pabloguadi_strlen')) {
	function pabloguadi_strlen($text) {
		return PABLOGUADI_MULTIBYTE ? mb_strlen($text) : strlen($text);
	}
}

if (!function_exists('pabloguadi_strpos')) {
	function pabloguadi_strpos($text, $char, $from=0) {
		return PABLOGUADI_MULTIBYTE ? mb_strpos($text, $char, $from) : strpos($text, $char, $from);
	}
}

if (!function_exists('pabloguadi_strrpos')) {
	function pabloguadi_strrpos($text, $char, $from=0) {
		return PABLOGUADI_MULTIBYTE ? mb_strrpos($text, $char, $from) : strrpos($text, $char, $from);
	}
}

if (!function_exists('pabloguadi_substr')) {
	function pabloguadi_substr($text, $from, $len=-999999) {
		if ($len==-999999) { 
			if ($from < 0)
				$len = -$from; 
			else
				$len = pabloguadi_strlen($text)-$from;
		}
		return PABLOGUADI_MULTIBYTE ? mb_substr($text, $from, $len) : substr($text, $from, $len);
	}
}

if (!function_exists('pabloguadi_strtolower')) {
	function pabloguadi_strtolower($text) {
		return PABLOGUADI_MULTIBYTE ? mb_strtolower($text) : strtolower($text);
	}
}

if (!function_exists('pabloguadi_strtoupper')) {
	function pabloguadi_strtoupper($text) {
		return PABLOGUADI_MULTIBYTE ? mb_strtoupper($text) : strtoupper($text);
	}
}

if (!function_exists('pabloguadi_strtoproper')) {
	function pabloguadi_strtoproper($text) { 
		$rez = ''; $last = ' ';
		for ($i=0; $i<pabloguadi_strlen($text); $i++) {
			$ch = pabloguadi_substr($text, $i, 1);
			$rez .= pabloguadi_strpos(' .,:;?!()[]{}+=', $last)!==false ? pabloguadi_strtoupper($ch) : pabloguadi_strtolower($ch);
			$last = $ch;
		}
		return $rez;
	}
}

if (!function_exists('pabloguadi_strrepeat')) {
	function pabloguadi_strrepeat($str, $n) {
		$rez = '';
		for ($i=0; $i<$n; $i++)
			$rez .= $str;
		return $rez;
	}
}

if (!function_exists('pabloguadi_strshort')) {
	function pabloguadi_strshort($str, $maxlength, $add='...') {
		if ($maxlength < 0) 
			return $str;
		if ($maxlength == 0) 
			return '';
		if ($maxlength >= pabloguadi_strlen($str)) 
			return strip_tags($str);
		$str = pabloguadi_substr(strip_tags($str), 0, $maxlength - pabloguadi_strlen($add));
		$ch = pabloguadi_substr($str, $maxlength - pabloguadi_strlen($add), 1);
		if ($ch != ' ') {
			for ($i = pabloguadi_strlen($str) - 1; $i > 0; $i--)
				if (pabloguadi_substr($str, $i, 1) == ' ') break;
			$str = trim(pabloguadi_substr($str, 0, $i));
		}
		if (!empty($str) && pabloguadi_strpos(',.:;-', pabloguadi_substr($str, -1))!==false) $str = pabloguadi_substr($str, 0, -1);
		return ($str) . ($add);
	}
}

// Clear string from spaces, line breaks and tags (only around text)
if (!function_exists('pabloguadi_strclear')) {
	function pabloguadi_strclear($text, $tags=array()) {
		if (empty($text)) return $text;
		if (!is_array($tags)) {
			if ($tags != '')
				$tags = explode($tags, ',');
			else
				$tags = array();
		}
		$text = trim(chop($text));
		if (is_array($tags) && count($tags) > 0) {
			foreach ($tags as $tag) {
				$open  = '<'.esc_attr($tag);
				$close = '</'.esc_attr($tag).'>';
				if (pabloguadi_substr($text, 0, pabloguadi_strlen($open))==$open) {
					$pos = pabloguadi_strpos($text, '>');
					if ($pos!==false) $text = pabloguadi_substr($text, $pos+1);
				}
				if (pabloguadi_substr($text, -pabloguadi_strlen($close))==$close) $text = pabloguadi_substr($text, 0, pabloguadi_strlen($text) - pabloguadi_strlen($close));
				$text = trim(chop($text));
			}
		}
		return $text;
	}
}

// Return slug for the any title string
if (!function_exists('pabloguadi_get_slug')) {
	function pabloguadi_get_slug($title) {
		return pabloguadi_strtolower(str_replace(array('\\','/','-',' ','.'), '_', $title));
	}
}

// Replace macros in the string
if (!function_exists('pabloguadi_strmacros')) {
	function pabloguadi_strmacros($str) {
		return str_replace(array("{{", "}}", "((", "))", "||"), array("<i>", "</i>", "<b>", "</b>", "<br>"), $str);
	}
}

// Unserialize string (try replace \n with \r\n)
if (!function_exists('pabloguadi_unserialize')) {
	function pabloguadi_unserialize($str) {
		if ( is_serialized($str) ) {
			try {
				$data = unserialize($str);
			} catch (Exception $e) {
				dcl($e->getMessage());
				$data = false;
			}
			if ($data===false) {
				try {
					$data = @unserialize(str_replace("\n", "\r\n", $str));
				} catch (Exception $e) {
					dcl($e->getMessage());
					$data = false;
				}
			}
			return $data;
		} else
			return $str;
	}
}
?>