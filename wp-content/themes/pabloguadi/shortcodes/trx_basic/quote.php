<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('pabloguadi_sc_quote_theme_setup')) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_sc_quote_theme_setup' );
	function pabloguadi_sc_quote_theme_setup() {
		add_action('pabloguadi_action_shortcodes_list', 		'pabloguadi_sc_quote_reg_shortcodes');
		if (function_exists('pabloguadi_exists_visual_composer') && pabloguadi_exists_visual_composer())
			add_action('pabloguadi_action_shortcodes_list_vc','pabloguadi_sc_quote_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_quote id="unique_id" cite="url" title=""]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/quote]
*/

if (!function_exists('pabloguadi_sc_quote')) {	
	function pabloguadi_sc_quote($atts, $content=null){	
		if (pabloguadi_in_shortcode_blogger()) return '';
		extract(pabloguadi_html_decode(shortcode_atts(array(
			// Individual params
			"title" => "",
			"cite" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . pabloguadi_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= pabloguadi_get_css_dimensions_from_values($width);
		$cite_param = $cite != '' ? ' cite="'.esc_attr($cite).'"' : '';
		$title = $title=='' ? $cite : $title;
		$content = do_shortcode($content);
		if (pabloguadi_substr($content, 0, 2)!='<p') $content = '<p>' . ($content) . '</p>';
		$output = '<blockquote' 
			. ($id ? ' id="'.esc_attr($id).'"' : '') . ($cite_param) 
			. ' class="sc_quote'. (!empty($class) ? ' '.esc_attr($class) : '').'"' 
			. (!pabloguadi_param_is_off($animation) ? ' data-animation="'.esc_attr(pabloguadi_get_animation_classes($animation)).'"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
			. '>'
				. ($content)
				. ($title == '' ? '' : ('<p class="sc_quote_title">' . ($cite!='' ? '<a href="'.esc_url($cite).'">' : '') . ($title) . ($cite!='' ? '</a>' : '') . '</p>'))
			.'</blockquote>';
		return apply_filters('pabloguadi_shortcode_output', $output, 'trx_quote', $atts, $content);
	}
	pabloguadi_require_shortcode('trx_quote', 'pabloguadi_sc_quote');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_quote_reg_shortcodes' ) ) {
	//add_action('pabloguadi_action_shortcodes_list', 'pabloguadi_sc_quote_reg_shortcodes');
	function pabloguadi_sc_quote_reg_shortcodes() {
	
		pabloguadi_sc_map("trx_quote", array(
			"title" => esc_html__("Quote", 'pabloguadi'),
			"desc" => wp_kses_data( __("Quote text", 'pabloguadi') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"cite" => array(
					"title" => esc_html__("Quote cite", 'pabloguadi'),
					"desc" => wp_kses_data( __("URL for quote cite", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"title" => array(
					"title" => esc_html__("Title (author)", 'pabloguadi'),
					"desc" => wp_kses_data( __("Quote title (author name)", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"_content_" => array(
					"title" => esc_html__("Quote content", 'pabloguadi'),
					"desc" => wp_kses_data( __("Quote content", 'pabloguadi') ),
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"width" => pabloguadi_shortcodes_width(),
				"top" => pabloguadi_get_sc_param('top'),
				"bottom" => pabloguadi_get_sc_param('bottom'),
				"left" => pabloguadi_get_sc_param('left'),
				"right" => pabloguadi_get_sc_param('right'),
				"id" => pabloguadi_get_sc_param('id'),
				"class" => pabloguadi_get_sc_param('class'),
				"animation" => pabloguadi_get_sc_param('animation'),
				"css" => pabloguadi_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_quote_reg_shortcodes_vc' ) ) {
	//add_action('pabloguadi_action_shortcodes_list_vc', 'pabloguadi_sc_quote_reg_shortcodes_vc');
	function pabloguadi_sc_quote_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_quote",
			"name" => esc_html__("Quote", 'pabloguadi'),
			"description" => wp_kses_data( __("Quote text", 'pabloguadi') ),
			"category" => esc_html__('Content', 'pabloguadi'),
			'icon' => 'icon_trx_quote',
			"class" => "trx_sc_single trx_sc_quote",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "cite",
					"heading" => esc_html__("Quote cite", 'pabloguadi'),
					"description" => wp_kses_data( __("URL for the quote cite link", 'pabloguadi') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title (author)", 'pabloguadi'),
					"description" => wp_kses_data( __("Quote title (author name)", 'pabloguadi') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "content",
					"heading" => esc_html__("Quote content", 'pabloguadi'),
					"description" => wp_kses_data( __("Quote content", 'pabloguadi') ),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				),
				pabloguadi_get_vc_param('id'),
				pabloguadi_get_vc_param('class'),
				pabloguadi_get_vc_param('animation'),
				pabloguadi_get_vc_param('css'),
				pabloguadi_vc_width(),
				pabloguadi_get_vc_param('margin_top'),
				pabloguadi_get_vc_param('margin_bottom'),
				pabloguadi_get_vc_param('margin_left'),
				pabloguadi_get_vc_param('margin_right')
			),
			'js_view' => 'VcTrxTextView'
		) );
		
		class WPBakeryShortCode_Trx_Quote extends PABLOGUADI_VC_ShortCodeSingle {}
	}
}
?>