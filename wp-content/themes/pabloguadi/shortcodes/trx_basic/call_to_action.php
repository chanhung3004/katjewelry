<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('pabloguadi_sc_call_to_action_theme_setup')) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_sc_call_to_action_theme_setup' );
	function pabloguadi_sc_call_to_action_theme_setup() {
		add_action('pabloguadi_action_shortcodes_list', 		'pabloguadi_sc_call_to_action_reg_shortcodes');
		if (function_exists('pabloguadi_exists_visual_composer') && pabloguadi_exists_visual_composer())
			add_action('pabloguadi_action_shortcodes_list_vc','pabloguadi_sc_call_to_action_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_call_to_action id="unique_id" style="1|2" align="left|center|right"]
	[inner shortcodes]
[/trx_call_to_action]
*/

if (!function_exists('pabloguadi_sc_call_to_action')) {	
	function pabloguadi_sc_call_to_action($atts, $content=null){	
		if (pabloguadi_in_shortcode_blogger()) return '';
		extract(pabloguadi_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "1",
			"align" => "center",
			"custom" => "no",
			"accent" => "no",
			"image" => "",
			"video" => "",
			"title" => "",
			"subtitle" => "",
			"description" => "",
			"link" => '',
			"link_caption" => esc_html__('Learn more', 'pabloguadi'),
			"link2" => '',
			"link2_caption" => '',
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
	
		if (empty($id)) $id = "sc_call_to_action_".str_replace('.', '', mt_rand());
		if (empty($width)) $width = "100%";
	
		if ($image > 0) {
			$attach = wp_get_attachment_image_src( $image, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$image = $attach[0];
		}
		if (!empty($image)) {
			$thumb_sizes = pabloguadi_get_thumb_sizes(array('layout' => 'excerpt'));
			$image = !empty($video) 
				? pabloguadi_get_resized_image_url($image, $thumb_sizes['w'], $thumb_sizes['h']) 
				: pabloguadi_get_resized_image_tag($image, $thumb_sizes['w'], $thumb_sizes['h']);
		}
	
		if (!empty($video)) {
			$video = '<video' . ($id ? ' id="' . esc_attr($id.'_video') . '"' : '') 
				. ' class="sc_video"'
				. ' src="' . esc_url(pabloguadi_get_video_player_url($video)) . '"'
				. ' width="' . esc_attr($width) . '" height="' . esc_attr($height) . '"' 
				. ' data-width="' . esc_attr($width) . '" data-height="' . esc_attr($height) . '"' 
				. ' data-ratio="16:9"'
				. ($image ? ' poster="'.esc_attr($image).'" data-image="'.esc_attr($image).'"' : '') 
				. ' controls="controls" loop="loop"'
				. '>'
				. '</video>';
			if (pabloguadi_get_custom_option('substitute_video')=='no') {
				$video = pabloguadi_get_video_frame($video, $image, '', '');
			} else {
				if ((isset($_GET['vc_editable']) && $_GET['vc_editable']=='true') && (isset($_POST['action']) && $_POST['action']=='vc_load_shortcode')) {
					$video = pabloguadi_substitute_video($video, $width, $height, false);
				}
			}
			if (pabloguadi_get_theme_option('use_mediaelement')=='yes')
				pabloguadi_enqueue_script('wp-mediaelement');
		}
		
		$class .= ($class ? ' ' : '') . pabloguadi_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= pabloguadi_get_css_dimensions_from_values($width, $height);
		
		$content = do_shortcode($content);
		
		$featured = ($style==1 && (!empty($content) || !empty($image) || !empty($video))
					? '<div class="sc_call_to_action_featured column-1_2">'
						. (!empty($content) 
							? $content 
							: (!empty($video) 
								? $video 
								: $image)
							)
						. '</div>'
					: '');
	
		$need_columns = ($featured || $style==2) && !in_array($align, array('center', 'none'))
							? ($style==2 ? 4 : 2)
							: 0;
		
		$buttons = (!empty($link) || !empty($link2) 
						? '<div class="sc_call_to_action_buttons sc_item_buttons'.($need_columns && $style==2 ? ' column-1_'.esc_attr($need_columns) : '').'">'
							. (!empty($link) 
								? '<div class="sc_call_to_action_button sc_item_button">'.do_shortcode('[trx_button link="'.esc_url($link).'" icon="icon-right"]'.esc_html($link_caption).'[/trx_button]').'</div>' 
								: '')
							. (!empty($link2) 
								? '<div class="sc_call_to_action_button sc_item_button">'.do_shortcode('[trx_button link="'.esc_url($link2).'" icon="icon-right"]'.esc_html($link2_caption).'[/trx_button]').'</div>' 
								: '')
							. '</div>'
						: '');
	
		
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_call_to_action'
					. (pabloguadi_param_is_on($accent) ? ' sc_call_to_action_accented' : '')
					. ' sc_call_to_action_style_' . esc_attr($style) 
					. ' sc_call_to_action_align_'.esc_attr($align)
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. '"'
				. (!pabloguadi_param_is_off($animation) ? ' data-animation="'.esc_attr(pabloguadi_get_animation_classes($animation)).'"' : '')
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. '>'
				. ($need_columns ? '<div class="columns_wrap">' : '')
				. ($align!='right' ? $featured : '')
				. ($style==2 && $align=='right' ? $buttons : '')
				. '<div class="sc_call_to_action_info'.($need_columns ? ' column-'.esc_attr($need_columns-1).'_'.esc_attr($need_columns) : '').'">'
					. (!empty($subtitle) ? '<h6 class="sc_call_to_action_subtitle sc_item_subtitle">' . trim(pabloguadi_strmacros($subtitle)) . '</h6>' : '')
					. (!empty($title) ? '<h2 class="sc_call_to_action_title sc_item_title' . (empty($description) ? ' sc_item_title_without_descr' : ' sc_item_title_with_descr') . '">' . trim(pabloguadi_strmacros($title)) . '</h2>' : '')
					. (!empty($description) ? '<div class="sc_call_to_action_descr sc_item_descr">' . trim(pabloguadi_strmacros($description)) . '</div>' : '')
					. ($style==1 ? $buttons : '')
				. '</div>'
				. ($style==2 && $align!='right' ? $buttons : '')
				. ($align=='right' ? $featured : '')
				. ($need_columns ? '</div>' : '')
			. '</div>';
	
		return apply_filters('pabloguadi_shortcode_output', $output, 'trx_call_to_action', $atts, $content);
	}
	pabloguadi_require_shortcode('trx_call_to_action', 'pabloguadi_sc_call_to_action');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_call_to_action_reg_shortcodes' ) ) {
	//add_action('pabloguadi_action_shortcodes_list', 'pabloguadi_sc_call_to_action_reg_shortcodes');
	function pabloguadi_sc_call_to_action_reg_shortcodes() {
	
		pabloguadi_sc_map("trx_call_to_action", array(
			"title" => esc_html__("Call to action", 'pabloguadi'),
			"desc" => wp_kses_data( __("Insert call to action block in your page (post)", 'pabloguadi') ),
			"decorate" => true,
			"container" => true,
			"params" => array(
				"title" => array(
					"title" => esc_html__("Title", 'pabloguadi'),
					"desc" => wp_kses_data( __("Title for the block", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"subtitle" => array(
					"title" => esc_html__("Subtitle", 'pabloguadi'),
					"desc" => wp_kses_data( __("Subtitle for the block", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"description" => array(
					"title" => esc_html__("Description", 'pabloguadi'),
					"desc" => wp_kses_data( __("Short description for the block", 'pabloguadi') ),
					"value" => "",
					"type" => "textarea"
				),
				"style" => array(
					"title" => esc_html__("Style", 'pabloguadi'),
					"desc" => wp_kses_data( __("Select style to display block", 'pabloguadi') ),
					"value" => "1",
					"type" => "checklist",
					"options" => pabloguadi_get_list_styles(1, 2)
				),
				"align" => array(
					"title" => esc_html__("Alignment", 'pabloguadi'),
					"desc" => wp_kses_data( __("Alignment elements in the block", 'pabloguadi') ),
					"value" => "",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => pabloguadi_get_sc_param('align')
				),
				"accent" => array(
					"title" => esc_html__("Accented", 'pabloguadi'),
					"desc" => wp_kses_data( __("Fill entire block with links color from current color scheme", 'pabloguadi') ),
					"divider" => true,
					"value" => "no",
					"type" => "switch",
					"options" => pabloguadi_get_sc_param('yes_no')
				),
				"custom" => array(
					"title" => esc_html__("Custom", 'pabloguadi'),
					"desc" => wp_kses_data( __("Allow get featured image or video from inner shortcodes (custom) or get it from shortcode parameters below", 'pabloguadi') ),
					"divider" => true,
					"value" => "no",
					"type" => "switch",
					"options" => pabloguadi_get_sc_param('yes_no')
				),
				"image" => array(
					"title" => esc_html__("Image", 'pabloguadi'),
					"desc" => wp_kses_data( __("Select or upload image or write URL from other site to include image into this block", 'pabloguadi') ),
					"divider" => true,
					"readonly" => false,
					"value" => "",
					"type" => "media"
				),
				"video" => array(
					"title" => esc_html__("URL for video file", 'pabloguadi'),
					"desc" => wp_kses_data( __("Select video from media library or paste URL for video file from other site to include video into this block", 'pabloguadi') ),
					"readonly" => false,
					"value" => "",
					"type" => "media",
					"before" => array(
						'title' => esc_html__('Choose video', 'pabloguadi'),
						'action' => 'media_upload',
						'type' => 'video',
						'multiple' => false,
						'linked_field' => '',
						'captions' => array( 	
							'choose' => esc_html__('Choose video file', 'pabloguadi'),
							'update' => esc_html__('Select video file', 'pabloguadi')
						)
					),
					"after" => array(
						'icon' => 'icon-cancel',
						'action' => 'media_reset'
					)
				),
				"link" => array(
					"title" => esc_html__("Button URL", 'pabloguadi'),
					"desc" => wp_kses_data( __("Link URL for the button at the bottom of the block", 'pabloguadi') ),
					"divider" => true,
					"value" => "",
					"type" => "text"
				),
				"link_caption" => array(
					"title" => esc_html__("Button caption", 'pabloguadi'),
					"desc" => wp_kses_data( __("Caption for the button at the bottom of the block", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"link2" => array(
					"title" => esc_html__("Button 2 URL", 'pabloguadi'),
					"desc" => wp_kses_data( __("Link URL for the second button at the bottom of the block", 'pabloguadi') ),
					"divider" => true,
					"value" => "",
					"type" => "text"
				),
				"link2_caption" => array(
					"title" => esc_html__("Button 2 caption", 'pabloguadi'),
					"desc" => wp_kses_data( __("Caption for the second button at the bottom of the block", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"width" => pabloguadi_shortcodes_width(),
				"height" => pabloguadi_shortcodes_height(),
				"top" => pabloguadi_get_sc_param('top'),
				"bottom" => pabloguadi_get_sc_param('bottom'),
				"left" => pabloguadi_get_sc_param('left'),
				"right" => pabloguadi_get_sc_param('right'),
				"id" => pabloguadi_get_sc_param('id'),
				"class" => pabloguadi_get_sc_param('class'),
				"animation" => pabloguadi_get_sc_param('animation'),
				"css" => pabloguadi_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_call_to_action_reg_shortcodes_vc' ) ) {
	//add_action('pabloguadi_action_shortcodes_list_vc', 'pabloguadi_sc_call_to_action_reg_shortcodes_vc');
	function pabloguadi_sc_call_to_action_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_call_to_action",
			"name" => esc_html__("Call to Action", 'pabloguadi'),
			"description" => wp_kses_data( __("Insert call to action block in your page (post)", 'pabloguadi') ),
			"category" => esc_html__('Content', 'pabloguadi'),
			'icon' => 'icon_trx_call_to_action',
			"class" => "trx_sc_collection trx_sc_call_to_action",
			"content_element" => true,
			"is_container" => true,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "style",
					"heading" => esc_html__("Block's style", 'pabloguadi'),
					"description" => wp_kses_data( __("Select style to display this block", 'pabloguadi') ),
					"class" => "",
					"admin_label" => true,
					"value" => array_flip(pabloguadi_get_list_styles(1, 2)),
					"type" => "dropdown"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'pabloguadi'),
					"description" => wp_kses_data( __("Select block alignment", 'pabloguadi') ),
					"class" => "",
					"value" => array_flip(pabloguadi_get_sc_param('align')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "accent",
					"heading" => esc_html__("Accent", 'pabloguadi'),
					"description" => wp_kses_data( __("Fill entire block with links color from current color scheme", 'pabloguadi') ),
					"class" => "",
					"value" => array("Fill with links color" => "yes" ),
					"type" => "checkbox"
				),
				array(
					"param_name" => "custom",
					"heading" => esc_html__("Custom", 'pabloguadi'),
					"description" => wp_kses_data( __("Allow get featured image or video from inner shortcodes (custom) or get it from shortcode parameters below", 'pabloguadi') ),
					"class" => "",
					"value" => array("Custom content" => "yes" ),
					"type" => "checkbox"
				),
				array(
					"param_name" => "image",
					"heading" => esc_html__("Image", 'pabloguadi'),
					"description" => wp_kses_data( __("Image to display inside block", 'pabloguadi') ),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				),
				array(
					"param_name" => "video",
					"heading" => esc_html__("URL for video file", 'pabloguadi'),
					"description" => wp_kses_data( __("Paste URL for video file to display inside block", 'pabloguadi') ),
					'dependency' => array(
						'element' => 'custom',
						'is_empty' => true
					),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'pabloguadi'),
					"description" => wp_kses_data( __("Title for the block", 'pabloguadi') ),
					"admin_label" => true,
					"group" => esc_html__('Captions', 'pabloguadi'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "subtitle",
					"heading" => esc_html__("Subtitle", 'pabloguadi'),
					"description" => wp_kses_data( __("Subtitle for the block", 'pabloguadi') ),
					"group" => esc_html__('Captions', 'pabloguadi'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "description",
					"heading" => esc_html__("Description", 'pabloguadi'),
					"description" => wp_kses_data( __("Description for the block", 'pabloguadi') ),
					"group" => esc_html__('Captions', 'pabloguadi'),
					"class" => "",
					"value" => "",
					"type" => "textarea"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Button URL", 'pabloguadi'),
					"description" => wp_kses_data( __("Link URL for the button at the bottom of the block", 'pabloguadi') ),
					"group" => esc_html__('Captions', 'pabloguadi'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link_caption",
					"heading" => esc_html__("Button caption", 'pabloguadi'),
					"description" => wp_kses_data( __("Caption for the button at the bottom of the block", 'pabloguadi') ),
					"group" => esc_html__('Captions', 'pabloguadi'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link2",
					"heading" => esc_html__("Button 2 URL", 'pabloguadi'),
					"description" => wp_kses_data( __("Link URL for the second button at the bottom of the block", 'pabloguadi') ),
					"group" => esc_html__('Captions', 'pabloguadi'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link2_caption",
					"heading" => esc_html__("Button 2 caption", 'pabloguadi'),
					"description" => wp_kses_data( __("Caption for the second button at the bottom of the block", 'pabloguadi') ),
					"group" => esc_html__('Captions', 'pabloguadi'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				pabloguadi_get_vc_param('id'),
				pabloguadi_get_vc_param('class'),
				pabloguadi_get_vc_param('animation'),
				pabloguadi_get_vc_param('css'),
				pabloguadi_vc_width(),
				pabloguadi_vc_height(),
				pabloguadi_get_vc_param('margin_top'),
				pabloguadi_get_vc_param('margin_bottom'),
				pabloguadi_get_vc_param('margin_left'),
				pabloguadi_get_vc_param('margin_right')
			)
		) );
		
		class WPBakeryShortCode_Trx_Call_To_Action extends PABLOGUADI_VC_ShortCodeCollection {}
	}
}
?>