<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('pabloguadi_sc_title_theme_setup')) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_sc_title_theme_setup' );
	function pabloguadi_sc_title_theme_setup() {
		add_action('pabloguadi_action_shortcodes_list', 		'pabloguadi_sc_title_reg_shortcodes');
		if (function_exists('pabloguadi_exists_visual_composer') && pabloguadi_exists_visual_composer())
			add_action('pabloguadi_action_shortcodes_list_vc','pabloguadi_sc_title_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_title id="unique_id" style='regular|iconed' icon='' image='' background="on|off" type="1-6"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_title]
*/

if (!function_exists('pabloguadi_sc_title')) {	
	function pabloguadi_sc_title($atts, $content=null){	
		if (pabloguadi_in_shortcode_blogger()) return '';
		extract(pabloguadi_html_decode(shortcode_atts(array(
			// Individual params
			"type" => "1",
			"style" => "regular",
			"align" => "",
			"font_weight" => "",
			"font_size" => "",
			"color" => "",
			"icon" => "",
			"image" => "",
			"picture" => "",
			"image_size" => "small",
			"position" => "left",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . pabloguadi_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= pabloguadi_get_css_dimensions_from_values($width)
			.($align && $align!='none' && !pabloguadi_param_is_inherit($align) ? 'text-align:' . esc_attr($align) .';' : '')
			.($color ? 'color:' . esc_attr($color) .';' : '')
			.($font_weight && !pabloguadi_param_is_inherit($font_weight) ? 'font-weight:' . esc_attr($font_weight) .';' : '')
			.($font_size   ? 'font-size:' . esc_attr($font_size) .';' : '')
			;
		$type = min(6, max(1, $type));
		if ($picture > 0) {
			$attach = wp_get_attachment_image_src( $picture, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$picture = $attach[0];
		}
		$pic = $style!='iconed' 
			? '' 
			: '<span class="sc_title_icon sc_title_icon_'.esc_attr($position).'  sc_title_icon_'.esc_attr($image_size).($icon!='' && $icon!='none' ? ' '.esc_attr($icon) : '').'"'.'>'
				.($picture ? '<img src="'.esc_url($picture).'" alt="" />' : '')
				.(empty($picture) && $image && $image!='none' ? '<img src="'.esc_url(pabloguadi_strpos($image, 'http')===0 ? $image : pabloguadi_get_file_url('images/icons/'.($image).'.png')).'" alt="" />' : '')
				.'</span>';
		$output = '<h' . esc_attr($type) . ($id ? ' id="'.esc_attr($id).'"' : '')
				. ' class="sc_title sc_title_'.esc_attr($style)
					.($align && $align!='none' && !pabloguadi_param_is_inherit($align) ? ' sc_align_' . esc_attr($align) : '')
					.(!empty($class) ? ' '.esc_attr($class) : '')
					.'"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. (!pabloguadi_param_is_off($animation) ? ' data-animation="'.esc_attr(pabloguadi_get_animation_classes($animation)).'"' : '')
				. '>'
					. ($pic)
					. ($style=='divider' ? '<span class="sc_title_divider_before"'.($color ? ' style="background-color: '.esc_attr($color).'"' : '').'></span>' : '')
					. do_shortcode($content) 
					. ($style=='divider' ? '<span class="sc_title_divider_after"'.($color ? ' style="background-color: '.esc_attr($color).'"' : '').'></span>' : '')
				. '</h' . esc_attr($type) . '>';
		return apply_filters('pabloguadi_shortcode_output', $output, 'trx_title', $atts, $content);
	}
	pabloguadi_require_shortcode('trx_title', 'pabloguadi_sc_title');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_title_reg_shortcodes' ) ) {
	//add_action('pabloguadi_action_shortcodes_list', 'pabloguadi_sc_title_reg_shortcodes');
	function pabloguadi_sc_title_reg_shortcodes() {
	
		pabloguadi_sc_map("trx_title", array(
			"title" => esc_html__("Title", 'pabloguadi'),
			"desc" => wp_kses_data( __("Create header tag (1-6 level) with many styles", 'pabloguadi') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"_content_" => array(
					"title" => esc_html__("Title content", 'pabloguadi'),
					"desc" => wp_kses_data( __("Title content", 'pabloguadi') ),
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"type" => array(
					"title" => esc_html__("Title type", 'pabloguadi'),
					"desc" => wp_kses_data( __("Title type (header level)", 'pabloguadi') ),
					"divider" => true,
					"value" => "1",
					"type" => "select",
					"options" => array(
						'1' => esc_html__('Header 1', 'pabloguadi'),
						'2' => esc_html__('Header 2', 'pabloguadi'),
						'3' => esc_html__('Header 3', 'pabloguadi'),
						'4' => esc_html__('Header 4', 'pabloguadi'),
						'5' => esc_html__('Header 5', 'pabloguadi'),
						'6' => esc_html__('Header 6', 'pabloguadi'),
					)
				),
				"style" => array(
					"title" => esc_html__("Title style", 'pabloguadi'),
					"desc" => wp_kses_data( __("Title style", 'pabloguadi') ),
					"value" => "regular",
					"type" => "select",
					"options" => array(
						'regular' => esc_html__('Regular', 'pabloguadi'),
						'underline' => esc_html__('Underline', 'pabloguadi'),
						'divider' => esc_html__('Divider', 'pabloguadi'),
						'iconed' => esc_html__('With icon (image)', 'pabloguadi')
					)
				),
				"align" => array(
					"title" => esc_html__("Alignment", 'pabloguadi'),
					"desc" => wp_kses_data( __("Title text alignment", 'pabloguadi') ),
					"value" => "",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => pabloguadi_get_sc_param('align')
				), 
				"font_size" => array(
					"title" => esc_html__("Font_size", 'pabloguadi'),
					"desc" => wp_kses_data( __("Custom font size. If empty - use theme default", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"font_weight" => array(
					"title" => esc_html__("Font weight", 'pabloguadi'),
					"desc" => wp_kses_data( __("Custom font weight. If empty or inherit - use theme default", 'pabloguadi') ),
					"value" => "",
					"type" => "select",
					"size" => "medium",
					"options" => array(
						'inherit' => esc_html__('Default', 'pabloguadi'),
						'100' => esc_html__('Thin (100)', 'pabloguadi'),
						'300' => esc_html__('Light (300)', 'pabloguadi'),
						'400' => esc_html__('Normal (400)', 'pabloguadi'),
						'600' => esc_html__('Semibold (600)', 'pabloguadi'),
						'700' => esc_html__('Bold (700)', 'pabloguadi'),
						'900' => esc_html__('Black (900)', 'pabloguadi')
					)
				),
				"color" => array(
					"title" => esc_html__("Title color", 'pabloguadi'),
					"desc" => wp_kses_data( __("Select color for the title", 'pabloguadi') ),
					"value" => "",
					"type" => "color"
				),
				"icon" => array(
					"title" => esc_html__('Title font icon',  'pabloguadi'),
					"desc" => wp_kses_data( __("Select font icon for the title from Fontello icons set (if style=iconed)",  'pabloguadi') ),
					"dependency" => array(
						'style' => array('iconed')
					),
					"value" => "",
					"type" => "icons",
					"options" => pabloguadi_get_sc_param('icons')
				),
				"image" => array(
					"title" => esc_html__('or image icon',  'pabloguadi'),
					"desc" => wp_kses_data( __("Select image icon for the title instead icon above (if style=iconed)",  'pabloguadi') ),
					"dependency" => array(
						'style' => array('iconed')
					),
					"value" => "",
					"type" => "images",
					"size" => "small",
					"options" => pabloguadi_get_sc_param('images')
				),
				"picture" => array(
					"title" => esc_html__('or URL for image file', 'pabloguadi'),
					"desc" => wp_kses_data( __("Select or upload image or write URL from other site (if style=iconed)", 'pabloguadi') ),
					"dependency" => array(
						'style' => array('iconed')
					),
					"readonly" => false,
					"value" => "",
					"type" => "media"
				),
				"image_size" => array(
					"title" => esc_html__('Image (picture) size', 'pabloguadi'),
					"desc" => wp_kses_data( __("Select image (picture) size (if style='iconed')", 'pabloguadi') ),
					"dependency" => array(
						'style' => array('iconed')
					),
					"value" => "small",
					"type" => "checklist",
					"options" => array(
						'small' => esc_html__('Small', 'pabloguadi'),
						'medium' => esc_html__('Medium', 'pabloguadi'),
						'large' => esc_html__('Large', 'pabloguadi')
					)
				),
				"position" => array(
					"title" => esc_html__('Icon (image) position', 'pabloguadi'),
					"desc" => wp_kses_data( __("Select icon (image) position (if style=iconed)", 'pabloguadi') ),
					"dependency" => array(
						'style' => array('iconed')
					),
					"value" => "left",
					"type" => "checklist",
					"options" => array(
						'top' => esc_html__('Top', 'pabloguadi'),
						'left' => esc_html__('Left', 'pabloguadi')
					)
				),
				"top" => pabloguadi_get_sc_param('top'),
				"bottom" => pabloguadi_get_sc_param('bottom'),
				"left" => pabloguadi_get_sc_param('left'),
				"right" => pabloguadi_get_sc_param('right'),
				"id" => pabloguadi_get_sc_param('id'),
				"class" => pabloguadi_get_sc_param('class'),
				"animation" => pabloguadi_get_sc_param('animation'),
				"css" => pabloguadi_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_title_reg_shortcodes_vc' ) ) {
	//add_action('pabloguadi_action_shortcodes_list_vc', 'pabloguadi_sc_title_reg_shortcodes_vc');
	function pabloguadi_sc_title_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_title",
			"name" => esc_html__("Title", 'pabloguadi'),
			"description" => wp_kses_data( __("Create header tag (1-6 level) with many styles", 'pabloguadi') ),
			"category" => esc_html__('Content', 'pabloguadi'),
			'icon' => 'icon_trx_title',
			"class" => "trx_sc_single trx_sc_title",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "content",
					"heading" => esc_html__("Title content", 'pabloguadi'),
					"description" => wp_kses_data( __("Title content", 'pabloguadi') ),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				),
				array(
					"param_name" => "type",
					"heading" => esc_html__("Title type", 'pabloguadi'),
					"description" => wp_kses_data( __("Title type (header level)", 'pabloguadi') ),
					"admin_label" => true,
					"class" => "",
					"value" => array(
						esc_html__('Header 1', 'pabloguadi') => '1',
						esc_html__('Header 2', 'pabloguadi') => '2',
						esc_html__('Header 3', 'pabloguadi') => '3',
						esc_html__('Header 4', 'pabloguadi') => '4',
						esc_html__('Header 5', 'pabloguadi') => '5',
						esc_html__('Header 6', 'pabloguadi') => '6'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "style",
					"heading" => esc_html__("Title style", 'pabloguadi'),
					"description" => wp_kses_data( __("Title style: only text (regular) or with icon/image (iconed)", 'pabloguadi') ),
					"admin_label" => true,
					"class" => "",
					"value" => array(
						esc_html__('Regular', 'pabloguadi') => 'regular',
						esc_html__('Underline', 'pabloguadi') => 'underline',
						esc_html__('Divider', 'pabloguadi') => 'divider',
						esc_html__('With icon (image)', 'pabloguadi') => 'iconed'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'pabloguadi'),
					"description" => wp_kses_data( __("Title text alignment", 'pabloguadi') ),
					"admin_label" => true,
					"class" => "",
					"value" => array_flip(pabloguadi_get_sc_param('align')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "font_size",
					"heading" => esc_html__("Font size", 'pabloguadi'),
					"description" => wp_kses_data( __("Custom font size. If empty - use theme default", 'pabloguadi') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "font_weight",
					"heading" => esc_html__("Font weight", 'pabloguadi'),
					"description" => wp_kses_data( __("Custom font weight. If empty or inherit - use theme default", 'pabloguadi') ),
					"class" => "",
					"value" => array(
						esc_html__('Default', 'pabloguadi') => 'inherit',
						esc_html__('Thin (100)', 'pabloguadi') => '100',
						esc_html__('Light (300)', 'pabloguadi') => '300',
						esc_html__('Normal (400)', 'pabloguadi') => '400',
						esc_html__('Semibold (600)', 'pabloguadi') => '600',
						esc_html__('Bold (700)', 'pabloguadi') => '700',
						esc_html__('Black (900)', 'pabloguadi') => '900'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "color",
					"heading" => esc_html__("Title color", 'pabloguadi'),
					"description" => wp_kses_data( __("Select color for the title", 'pabloguadi') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "icon",
					"heading" => esc_html__("Title font icon", 'pabloguadi'),
					"description" => wp_kses_data( __("Select font icon for the title from Fontello icons set (if style=iconed)", 'pabloguadi') ),
					"class" => "",
					"group" => esc_html__('Icon &amp; Image', 'pabloguadi'),
					'dependency' => array(
						'element' => 'style',
						'value' => array('iconed')
					),
					"value" => pabloguadi_get_sc_param('icons'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "image",
					"heading" => esc_html__("or image icon", 'pabloguadi'),
					"description" => wp_kses_data( __("Select image icon for the title instead icon above (if style=iconed)", 'pabloguadi') ),
					"class" => "",
					"group" => esc_html__('Icon &amp; Image', 'pabloguadi'),
					'dependency' => array(
						'element' => 'style',
						'value' => array('iconed')
					),
					"value" => pabloguadi_get_sc_param('images'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "picture",
					"heading" => esc_html__("or select uploaded image", 'pabloguadi'),
					"description" => wp_kses_data( __("Select or upload image or write URL from other site (if style=iconed)", 'pabloguadi') ),
					"group" => esc_html__('Icon &amp; Image', 'pabloguadi'),
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				),
				array(
					"param_name" => "image_size",
					"heading" => esc_html__("Image (picture) size", 'pabloguadi'),
					"description" => wp_kses_data( __("Select image (picture) size (if style=iconed)", 'pabloguadi') ),
					"group" => esc_html__('Icon &amp; Image', 'pabloguadi'),
					"class" => "",
					"value" => array(
						esc_html__('Small', 'pabloguadi') => 'small',
						esc_html__('Medium', 'pabloguadi') => 'medium',
						esc_html__('Large', 'pabloguadi') => 'large'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "position",
					"heading" => esc_html__("Icon (image) position", 'pabloguadi'),
					"description" => wp_kses_data( __("Select icon (image) position (if style=iconed)", 'pabloguadi') ),
					"group" => esc_html__('Icon &amp; Image', 'pabloguadi'),
					"class" => "",
					"std" => "left",
					"value" => array(
						esc_html__('Top', 'pabloguadi') => 'top',
						esc_html__('Left', 'pabloguadi') => 'left'
					),
					"type" => "dropdown"
				),
				pabloguadi_get_vc_param('id'),
				pabloguadi_get_vc_param('class'),
				pabloguadi_get_vc_param('animation'),
				pabloguadi_get_vc_param('css'),
				pabloguadi_get_vc_param('margin_top'),
				pabloguadi_get_vc_param('margin_bottom'),
				pabloguadi_get_vc_param('margin_left'),
				pabloguadi_get_vc_param('margin_right')
			),
			'js_view' => 'VcTrxTextView'
		) );
		
		class WPBakeryShortCode_Trx_Title extends PABLOGUADI_VC_ShortCodeSingle {}
	}
}
?>