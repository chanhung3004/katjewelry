<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('pabloguadi_sc_hide_theme_setup')) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_sc_hide_theme_setup' );
	function pabloguadi_sc_hide_theme_setup() {
		add_action('pabloguadi_action_shortcodes_list', 		'pabloguadi_sc_hide_reg_shortcodes');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_hide selector="unique_id"]
*/

if (!function_exists('pabloguadi_sc_hide')) {	
	function pabloguadi_sc_hide($atts, $content=null){	
		if (pabloguadi_in_shortcode_blogger()) return '';
		extract(pabloguadi_html_decode(shortcode_atts(array(
			// Individual params
			"selector" => "",
			"hide" => "on",
			"delay" => 0
		), $atts)));
		$selector = trim(chop($selector));
		if (!empty($selector)) {
			pabloguadi_storage_concat('js_code', '
				'.($delay>0 ? 'setTimeout(function() {' : '').'
					jQuery("'.esc_attr($selector).'").' . ($hide=='on' ? 'hide' : 'show') . '();
				'.($delay>0 ? '},'.($delay).');' : '').'
			');
		}
		return apply_filters('pabloguadi_shortcode_output', $output, 'trx_hide', $atts, $content);
	}
	pabloguadi_require_shortcode('trx_hide', 'pabloguadi_sc_hide');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_hide_reg_shortcodes' ) ) {
	//add_action('pabloguadi_action_shortcodes_list', 'pabloguadi_sc_hide_reg_shortcodes');
	function pabloguadi_sc_hide_reg_shortcodes() {
	
		pabloguadi_sc_map("trx_hide", array(
			"title" => esc_html__("Hide/Show any block", 'pabloguadi'),
			"desc" => wp_kses_data( __("Hide or Show any block with desired CSS-selector", 'pabloguadi') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"selector" => array(
					"title" => esc_html__("Selector", 'pabloguadi'),
					"desc" => wp_kses_data( __("Any block's CSS-selector", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"hide" => array(
					"title" => esc_html__("Hide or Show", 'pabloguadi'),
					"desc" => wp_kses_data( __("New state for the block: hide or show", 'pabloguadi') ),
					"value" => "yes",
					"size" => "small",
					"options" => pabloguadi_get_sc_param('yes_no'),
					"type" => "switch"
				)
			)
		));
	}
}
?>