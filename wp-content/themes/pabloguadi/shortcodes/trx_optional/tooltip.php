<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('pabloguadi_sc_tooltip_theme_setup')) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_sc_tooltip_theme_setup' );
	function pabloguadi_sc_tooltip_theme_setup() {
		add_action('pabloguadi_action_shortcodes_list', 		'pabloguadi_sc_tooltip_reg_shortcodes');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_tooltip id="unique_id" title="Tooltip text here"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/tooltip]
*/

if (!function_exists('pabloguadi_sc_tooltip')) {	
	function pabloguadi_sc_tooltip($atts, $content=null){	
		if (pabloguadi_in_shortcode_blogger()) return '';
		extract(pabloguadi_html_decode(shortcode_atts(array(
			// Individual params
			"title" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		$output = '<span' . ($id ? ' id="'.esc_attr($id).'"' : '') 
					. ' class="sc_tooltip_parent'. (!empty($class) ? ' '.esc_attr($class) : '').'"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
					. '>'
						. do_shortcode($content)
						. '<span class="sc_tooltip">' . ($title) . '</span>'
					. '</span>';
		return apply_filters('pabloguadi_shortcode_output', $output, 'trx_tooltip', $atts, $content);
	}
	pabloguadi_require_shortcode('trx_tooltip', 'pabloguadi_sc_tooltip');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_tooltip_reg_shortcodes' ) ) {
	//add_action('pabloguadi_action_shortcodes_list', 'pabloguadi_sc_tooltip_reg_shortcodes');
	function pabloguadi_sc_tooltip_reg_shortcodes() {
	
		pabloguadi_sc_map("trx_tooltip", array(
			"title" => esc_html__("Tooltip", 'pabloguadi'),
			"desc" => wp_kses_data( __("Create tooltip for selected text", 'pabloguadi') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"title" => array(
					"title" => esc_html__("Title", 'pabloguadi'),
					"desc" => wp_kses_data( __("Tooltip title (required)", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"_content_" => array(
					"title" => esc_html__("Tipped content", 'pabloguadi'),
					"desc" => wp_kses_data( __("Highlighted content with tooltip", 'pabloguadi') ),
					"divider" => true,
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"id" => pabloguadi_get_sc_param('id'),
				"class" => pabloguadi_get_sc_param('class'),
				"css" => pabloguadi_get_sc_param('css')
			)
		));
	}
}
?>