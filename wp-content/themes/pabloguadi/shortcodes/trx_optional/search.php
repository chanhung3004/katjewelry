<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('pabloguadi_sc_search_theme_setup')) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_sc_search_theme_setup' );
	function pabloguadi_sc_search_theme_setup() {
		add_action('pabloguadi_action_shortcodes_list', 		'pabloguadi_sc_search_reg_shortcodes');
		if (function_exists('pabloguadi_exists_visual_composer') && pabloguadi_exists_visual_composer())
			add_action('pabloguadi_action_shortcodes_list_vc','pabloguadi_sc_search_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_search id="unique_id" open="yes|no"]
*/

if (!function_exists('pabloguadi_sc_search')) {	
	function pabloguadi_sc_search($atts, $content=null){	
		if (pabloguadi_in_shortcode_blogger()) return '';
		extract(pabloguadi_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "",
			"state" => "",
			"ajax" => "",
			"title" => esc_html__('Search', 'pabloguadi'),
			"scheme" => "original",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . pabloguadi_get_css_position_as_classes($top, $right, $bottom, $left);
		if ($style == 'fullscreen') {
			if (empty($ajax)) $ajax = "no";
			if (empty($state)) $state = "closed";
		} else if ($style == 'expand') {
			if (empty($ajax)) $ajax = pabloguadi_get_theme_option('use_ajax_search');
			if (empty($state)) $state = "closed";
		} else if ($style == 'slide') {
			if (empty($ajax)) $ajax = pabloguadi_get_theme_option('use_ajax_search');
			if (empty($state)) $state = "closed";
		} else {
			if (empty($ajax)) $ajax = pabloguadi_get_theme_option('use_ajax_search');
			if (empty($state)) $state = "fixed";
		}
		// Load core messages
		pabloguadi_enqueue_messages();
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') . ' class="search_wrap search_style_'.esc_attr($style).' search_state_'.esc_attr($state)
						. (pabloguadi_param_is_on($ajax) ? ' search_ajax' : '')
						. ($class ? ' '.esc_attr($class) : '')
						. '"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
					. (!pabloguadi_param_is_off($animation) ? ' data-animation="'.esc_attr(pabloguadi_get_animation_classes($animation)).'"' : '')
					. '>
						<div class="search_form_wrap">
							<form role="search" method="get" class="search_form" action="' . esc_url(home_url('/')) . '">
								<button type="submit" class="search_submit icon-search" title="' . ($state=='closed' ? esc_attr__('Open search', 'pabloguadi') : esc_attr__('Start search', 'pabloguadi')) . '"></button>
								<input type="text" class="search_field" placeholder="' . esc_attr($title) . '" value="' . esc_attr(get_search_query()) . '" name="s" />'
								. ($style == 'fullscreen' ? '<a class="search_close icon-cancel"></a>' : '')
							. '</form>
						</div>'
						. (pabloguadi_param_is_on($ajax) ? '<div class="search_results widget_area' . ($scheme && !pabloguadi_param_is_off($scheme) && !pabloguadi_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '') . '"><a class="search_results_close icon-cancel"></a><div class="search_results_content"></div></div>' : '')
					. '</div>';
		return apply_filters('pabloguadi_shortcode_output', $output, 'trx_search', $atts, $content);
	}
	pabloguadi_require_shortcode('trx_search', 'pabloguadi_sc_search');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_search_reg_shortcodes' ) ) {
	//add_action('pabloguadi_action_shortcodes_list', 'pabloguadi_sc_search_reg_shortcodes');
	function pabloguadi_sc_search_reg_shortcodes() {
	
		pabloguadi_sc_map("trx_search", array(
			"title" => esc_html__("Search", 'pabloguadi'),
			"desc" => wp_kses_data( __("Show search form", 'pabloguadi') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"style" => array(
					"title" => esc_html__("Style", 'pabloguadi'),
					"desc" => wp_kses_data( __("Select style to display search field", 'pabloguadi') ),
					"value" => "regular",
					"options" => pabloguadi_get_list_search_styles(),
					"type" => "checklist"
				),
				"state" => array(
					"title" => esc_html__("State", 'pabloguadi'),
					"desc" => wp_kses_data( __("Select search field initial state", 'pabloguadi') ),
					"value" => "fixed",
					"options" => array(
						"fixed"  => esc_html__('Fixed',  'pabloguadi'),
						"opened" => esc_html__('Opened', 'pabloguadi'),
						"closed" => esc_html__('Closed', 'pabloguadi')
					),
					"type" => "checklist"
				),
				"title" => array(
					"title" => esc_html__("Title", 'pabloguadi'),
					"desc" => wp_kses_data( __("Title (placeholder) for the search field", 'pabloguadi') ),
					"value" => esc_html__("Search &hellip;", 'pabloguadi'),
					"type" => "text"
				),
				"ajax" => array(
					"title" => esc_html__("AJAX", 'pabloguadi'),
					"desc" => wp_kses_data( __("Search via AJAX or reload page", 'pabloguadi') ),
					"value" => "yes",
					"options" => pabloguadi_get_sc_param('yes_no'),
					"type" => "switch"
				),
				"top" => pabloguadi_get_sc_param('top'),
				"bottom" => pabloguadi_get_sc_param('bottom'),
				"left" => pabloguadi_get_sc_param('left'),
				"right" => pabloguadi_get_sc_param('right'),
				"id" => pabloguadi_get_sc_param('id'),
				"class" => pabloguadi_get_sc_param('class'),
				"animation" => pabloguadi_get_sc_param('animation'),
				"css" => pabloguadi_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_search_reg_shortcodes_vc' ) ) {
	//add_action('pabloguadi_action_shortcodes_list_vc', 'pabloguadi_sc_search_reg_shortcodes_vc');
	function pabloguadi_sc_search_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_search",
			"name" => esc_html__("Search form", 'pabloguadi'),
			"description" => wp_kses_data( __("Insert search form", 'pabloguadi') ),
			"category" => esc_html__('Content', 'pabloguadi'),
			'icon' => 'icon_trx_search',
			"class" => "trx_sc_single trx_sc_search",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "style",
					"heading" => esc_html__("Style", 'pabloguadi'),
					"description" => wp_kses_data( __("Select style to display search field", 'pabloguadi') ),
					"class" => "",
					"value" => pabloguadi_get_list_search_styles(),
					"type" => "dropdown"
				),
				array(
					"param_name" => "state",
					"heading" => esc_html__("State", 'pabloguadi'),
					"description" => wp_kses_data( __("Select search field initial state", 'pabloguadi') ),
					"class" => "",
					"value" => array(
						esc_html__('Fixed', 'pabloguadi')  => "fixed",
						esc_html__('Opened', 'pabloguadi') => "opened",
						esc_html__('Closed', 'pabloguadi') => "closed"
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'pabloguadi'),
					"description" => wp_kses_data( __("Title (placeholder) for the search field", 'pabloguadi') ),
					"admin_label" => true,
					"class" => "",
					"value" => esc_html__("Search &hellip;", 'pabloguadi'),
					"type" => "textfield"
				),
				array(
					"param_name" => "ajax",
					"heading" => esc_html__("AJAX", 'pabloguadi'),
					"description" => wp_kses_data( __("Search via AJAX or reload page", 'pabloguadi') ),
					"class" => "",
					"value" => array(esc_html__('Use AJAX search', 'pabloguadi') => 'yes'),
					"type" => "checkbox"
				),
				pabloguadi_get_vc_param('id'),
				pabloguadi_get_vc_param('class'),
				pabloguadi_get_vc_param('animation'),
				pabloguadi_get_vc_param('css'),
				pabloguadi_get_vc_param('margin_top'),
				pabloguadi_get_vc_param('margin_bottom'),
				pabloguadi_get_vc_param('margin_left'),
				pabloguadi_get_vc_param('margin_right')
			)
		) );
		
		class WPBakeryShortCode_Trx_Search extends PABLOGUADI_VC_ShortCodeSingle {}
	}
}
?>