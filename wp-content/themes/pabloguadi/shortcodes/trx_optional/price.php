<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('pabloguadi_sc_price_theme_setup')) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_sc_price_theme_setup' );
	function pabloguadi_sc_price_theme_setup() {
		add_action('pabloguadi_action_shortcodes_list', 		'pabloguadi_sc_price_reg_shortcodes');
		if (function_exists('pabloguadi_exists_visual_composer') && pabloguadi_exists_visual_composer())
			add_action('pabloguadi_action_shortcodes_list_vc','pabloguadi_sc_price_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_price id="unique_id" currency="$" money="29.99" period="monthly"]
*/

if (!function_exists('pabloguadi_sc_price')) {	
	function pabloguadi_sc_price($atts, $content=null){	
		if (pabloguadi_in_shortcode_blogger()) return '';
		extract(pabloguadi_html_decode(shortcode_atts(array(
			// Individual params
			"money" => "",
			"currency" => "$",
			"period" => "",
			"align" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$output = '';
		if (!empty($money)) {
			$class .= ($class ? ' ' : '') . pabloguadi_get_css_position_as_classes($top, $right, $bottom, $left);
			$m = explode('.', str_replace(',', '.', $money));
			$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
					. ' class="sc_price'
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
					. '"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
					. '>'
				. '<span class="sc_price_currency">'.($currency).'</span>'
				. '<span class="sc_price_money">'.($m[0]).'</span>'
				. (!empty($m[1]) ? '<span class="sc_price_info">' : '')
				. (!empty($m[1]) ? '<span class="sc_price_penny">'.($m[1]).'</span>' : '')
				. (!empty($period) ? '<span class="sc_price_period">'.($period).'</span>' : (!empty($m[1]) ? '<span class="sc_price_period_empty"></span>' : ''))
				. (!empty($m[1]) ? '</span>' : '')
				. '</div>';
		}
		return apply_filters('pabloguadi_shortcode_output', $output, 'trx_price', $atts, $content);
	}
	pabloguadi_require_shortcode('trx_price', 'pabloguadi_sc_price');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_price_reg_shortcodes' ) ) {
	//add_action('pabloguadi_action_shortcodes_list', 'pabloguadi_sc_price_reg_shortcodes');
	function pabloguadi_sc_price_reg_shortcodes() {
	
		pabloguadi_sc_map("trx_price", array(
			"title" => esc_html__("Price", 'pabloguadi'),
			"desc" => wp_kses_data( __("Insert price with decoration", 'pabloguadi') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"money" => array(
					"title" => esc_html__("Money", 'pabloguadi'),
					"desc" => wp_kses_data( __("Money value (dot or comma separated)", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"currency" => array(
					"title" => esc_html__("Currency", 'pabloguadi'),
					"desc" => wp_kses_data( __("Currency character", 'pabloguadi') ),
					"value" => "$",
					"type" => "text"
				),
				"period" => array(
					"title" => esc_html__("Period", 'pabloguadi'),
					"desc" => wp_kses_data( __("Period text (if need). For example: monthly, daily, etc.", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"align" => array(
					"title" => esc_html__("Alignment", 'pabloguadi'),
					"desc" => wp_kses_data( __("Align price to left or right side", 'pabloguadi') ),
					"value" => "",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => pabloguadi_get_sc_param('float')
				), 
				"top" => pabloguadi_get_sc_param('top'),
				"bottom" => pabloguadi_get_sc_param('bottom'),
				"left" => pabloguadi_get_sc_param('left'),
				"right" => pabloguadi_get_sc_param('right'),
				"id" => pabloguadi_get_sc_param('id'),
				"class" => pabloguadi_get_sc_param('class'),
				"css" => pabloguadi_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_price_reg_shortcodes_vc' ) ) {
	//add_action('pabloguadi_action_shortcodes_list_vc', 'pabloguadi_sc_price_reg_shortcodes_vc');
	function pabloguadi_sc_price_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_price",
			"name" => esc_html__("Price", 'pabloguadi'),
			"description" => wp_kses_data( __("Insert price with decoration", 'pabloguadi') ),
			"category" => esc_html__('Content', 'pabloguadi'),
			'icon' => 'icon_trx_price',
			"class" => "trx_sc_single trx_sc_price",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "money",
					"heading" => esc_html__("Money", 'pabloguadi'),
					"description" => wp_kses_data( __("Money value (dot or comma separated)", 'pabloguadi') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "currency",
					"heading" => esc_html__("Currency symbol", 'pabloguadi'),
					"description" => wp_kses_data( __("Currency character", 'pabloguadi') ),
					"admin_label" => true,
					"class" => "",
					"value" => "$",
					"type" => "textfield"
				),
				array(
					"param_name" => "period",
					"heading" => esc_html__("Period", 'pabloguadi'),
					"description" => wp_kses_data( __("Period text (if need). For example: monthly, daily, etc.", 'pabloguadi') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'pabloguadi'),
					"description" => wp_kses_data( __("Align price to left or right side", 'pabloguadi') ),
					"admin_label" => true,
					"class" => "",
					"value" => array_flip(pabloguadi_get_sc_param('float')),
					"type" => "dropdown"
				),
				pabloguadi_get_vc_param('id'),
				pabloguadi_get_vc_param('class'),
				pabloguadi_get_vc_param('css'),
				pabloguadi_get_vc_param('margin_top'),
				pabloguadi_get_vc_param('margin_bottom'),
				pabloguadi_get_vc_param('margin_left'),
				pabloguadi_get_vc_param('margin_right')
			)
		) );
		
		class WPBakeryShortCode_Trx_Price extends PABLOGUADI_VC_ShortCodeSingle {}
	}
}
?>