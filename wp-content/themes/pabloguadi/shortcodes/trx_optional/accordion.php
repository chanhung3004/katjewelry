<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('pabloguadi_sc_accordion_theme_setup')) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_sc_accordion_theme_setup' );
	function pabloguadi_sc_accordion_theme_setup() {
		add_action('pabloguadi_action_shortcodes_list', 		'pabloguadi_sc_accordion_reg_shortcodes');
		if (function_exists('pabloguadi_exists_visual_composer') && pabloguadi_exists_visual_composer())
			add_action('pabloguadi_action_shortcodes_list_vc','pabloguadi_sc_accordion_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_accordion counter="off" initial="1"]
	[trx_accordion_item title="Accordion Title 1"]Lorem ipsum dolor sit amet, consectetur adipisicing elit[/trx_accordion_item]
	[trx_accordion_item title="Accordion Title 2"]Proin dignissim commodo magna at luctus. Nam molestie justo augue, nec eleifend urna laoreet non.[/trx_accordion_item]
	[trx_accordion_item title="Accordion Title 3 with custom icons" icon_closed="icon-check" icon_opened="icon-delete"]Curabitur tristique tempus arcu a placerat.[/trx_accordion_item]
[/trx_accordion]
*/
if (!function_exists('pabloguadi_sc_accordion')) {	
	function pabloguadi_sc_accordion($atts, $content=null){	
		if (pabloguadi_in_shortcode_blogger()) return '';
		extract(pabloguadi_html_decode(shortcode_atts(array(
			// Individual params
			"initial" => "1",
			"counter" => "off",
			"icon_closed" => "icon-plus",
			"icon_opened" => "icon-minus",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . pabloguadi_get_css_position_as_classes($top, $right, $bottom, $left);
		$initial = max(0, (int) $initial);
		pabloguadi_storage_set('sc_accordion_data', array(
			'counter' => 0,
            'show_counter' => pabloguadi_param_is_on($counter),
            'icon_closed' => empty($icon_closed) || pabloguadi_param_is_inherit($icon_closed) ? "icon-plus" : $icon_closed,
            'icon_opened' => empty($icon_opened) || pabloguadi_param_is_inherit($icon_opened) ? "icon-minus" : $icon_opened
            )
        );
		pabloguadi_enqueue_script('jquery-ui-accordion', false, array('jquery','jquery-ui-core'), null, true);
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_accordion'
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. (pabloguadi_param_is_on($counter) ? ' sc_show_counter' : '') 
				. '"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
				. ' data-active="' . ($initial-1) . '"'
				. (!pabloguadi_param_is_off($animation) ? ' data-animation="'.esc_attr(pabloguadi_get_animation_classes($animation)).'"' : '')
				. '>'
				. do_shortcode($content)
				. '</div>';
		return apply_filters('pabloguadi_shortcode_output', $output, 'trx_accordion', $atts, $content);
	}
	pabloguadi_require_shortcode('trx_accordion', 'pabloguadi_sc_accordion');
}


if (!function_exists('pabloguadi_sc_accordion_item')) {	
	function pabloguadi_sc_accordion_item($atts, $content=null) {
		if (pabloguadi_in_shortcode_blogger()) return '';
		extract(pabloguadi_html_decode(shortcode_atts( array(
			// Individual params
			"icon_closed" => "",
			"icon_opened" => "",
			"title" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		pabloguadi_storage_inc_array('sc_accordion_data', 'counter');
		if (empty($icon_closed) || pabloguadi_param_is_inherit($icon_closed)) $icon_closed = pabloguadi_storage_get_array('sc_accordion_data', 'icon_closed', '', "icon-plus");
		if (empty($icon_opened) || pabloguadi_param_is_inherit($icon_opened)) $icon_opened = pabloguadi_storage_get_array('sc_accordion_data', 'icon_opened', '', "icon-minus");
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_accordion_item' 
				. (!empty($class) ? ' '.esc_attr($class) : '')
				. (pabloguadi_storage_get_array('sc_accordion_data', 'counter') % 2 == 1 ? ' odd' : ' even') 
				. (pabloguadi_storage_get_array('sc_accordion_data', 'counter') == 1 ? ' first' : '') 
				. '">'
				. '<h5 class="sc_accordion_title">'
				. (!pabloguadi_param_is_off($icon_closed) ? '<span class="sc_accordion_icon sc_accordion_icon_closed '.esc_attr($icon_closed).'"></span>' : '')
				. (!pabloguadi_param_is_off($icon_opened) ? '<span class="sc_accordion_icon sc_accordion_icon_opened '.esc_attr($icon_opened).'"></span>' : '')
				. (pabloguadi_storage_get_array('sc_accordion_data', 'show_counter') ? '<span class="sc_items_counter">'.(pabloguadi_storage_get_array('sc_accordion_data', 'counter')).'</span>' : '')
				. ($title)
				. '</h5>'
				. '<div class="sc_accordion_content"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
					. '>'
					. do_shortcode($content) 
				. '</div>'
				. '</div>';
		return apply_filters('pabloguadi_shortcode_output', $output, 'trx_accordion_item', $atts, $content);
	}
	pabloguadi_require_shortcode('trx_accordion_item', 'pabloguadi_sc_accordion_item');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_accordion_reg_shortcodes' ) ) {
	//add_action('pabloguadi_action_shortcodes_list', 'pabloguadi_sc_accordion_reg_shortcodes');
	function pabloguadi_sc_accordion_reg_shortcodes() {
	
		pabloguadi_sc_map("trx_accordion", array(
			"title" => esc_html__("Accordion", 'pabloguadi'),
			"desc" => wp_kses_data( __("Accordion items", 'pabloguadi') ),
			"decorate" => true,
			"container" => false,
			"params" => array(
				"counter" => array(
					"title" => esc_html__("Counter", 'pabloguadi'),
					"desc" => wp_kses_data( __("Display counter before each accordion title", 'pabloguadi') ),
					"value" => "off",
					"type" => "switch",
					"options" => pabloguadi_get_sc_param('on_off')
				),
				"initial" => array(
					"title" => esc_html__("Initially opened item", 'pabloguadi'),
					"desc" => wp_kses_data( __("Number of initially opened item", 'pabloguadi') ),
					"value" => 1,
					"min" => 0,
					"type" => "spinner"
				),
				"icon_closed" => array(
					"title" => esc_html__("Icon while closed",  'pabloguadi'),
					"desc" => wp_kses_data( __('Select icon for the closed accordion item from Fontello icons set',  'pabloguadi') ),
					"value" => "",
					"type" => "icons",
					"options" => pabloguadi_get_sc_param('icons')
				),
				"icon_opened" => array(
					"title" => esc_html__("Icon while opened",  'pabloguadi'),
					"desc" => wp_kses_data( __('Select icon for the opened accordion item from Fontello icons set',  'pabloguadi') ),
					"value" => "",
					"type" => "icons",
					"options" => pabloguadi_get_sc_param('icons')
				),
				"top" => pabloguadi_get_sc_param('top'),
				"bottom" => pabloguadi_get_sc_param('bottom'),
				"left" => pabloguadi_get_sc_param('left'),
				"right" => pabloguadi_get_sc_param('right'),
				"id" => pabloguadi_get_sc_param('id'),
				"class" => pabloguadi_get_sc_param('class'),
				"animation" => pabloguadi_get_sc_param('animation'),
				"css" => pabloguadi_get_sc_param('css')
			),
			"children" => array(
				"name" => "trx_accordion_item",
				"title" => esc_html__("Item", 'pabloguadi'),
				"desc" => wp_kses_data( __("Accordion item", 'pabloguadi') ),
				"container" => true,
				"params" => array(
					"title" => array(
						"title" => esc_html__("Accordion item title", 'pabloguadi'),
						"desc" => wp_kses_data( __("Title for current accordion item", 'pabloguadi') ),
						"value" => "",
						"type" => "text"
					),
					"icon_closed" => array(
						"title" => esc_html__("Icon while closed",  'pabloguadi'),
						"desc" => wp_kses_data( __('Select icon for the closed accordion item from Fontello icons set',  'pabloguadi') ),
						"value" => "",
						"type" => "icons",
						"options" => pabloguadi_get_sc_param('icons')
					),
					"icon_opened" => array(
						"title" => esc_html__("Icon while opened",  'pabloguadi'),
						"desc" => wp_kses_data( __('Select icon for the opened accordion item from Fontello icons set',  'pabloguadi') ),
						"value" => "",
						"type" => "icons",
						"options" => pabloguadi_get_sc_param('icons')
					),
					"_content_" => array(
						"title" => esc_html__("Accordion item content", 'pabloguadi'),
						"desc" => wp_kses_data( __("Current accordion item content", 'pabloguadi') ),
						"rows" => 4,
						"value" => "",
						"type" => "textarea"
					),
					"id" => pabloguadi_get_sc_param('id'),
					"class" => pabloguadi_get_sc_param('class'),
					"css" => pabloguadi_get_sc_param('css')
				)
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_accordion_reg_shortcodes_vc' ) ) {
	//add_action('pabloguadi_action_shortcodes_list_vc', 'pabloguadi_sc_accordion_reg_shortcodes_vc');
	function pabloguadi_sc_accordion_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_accordion",
			"name" => esc_html__("Accordion", 'pabloguadi'),
			"description" => wp_kses_data( __("Accordion items", 'pabloguadi') ),
			"category" => esc_html__('Content', 'pabloguadi'),
			'icon' => 'icon_trx_accordion',
			"class" => "trx_sc_collection trx_sc_accordion",
			"content_element" => true,
			"is_container" => true,
			"show_settings_on_create" => false,
			"as_parent" => array('only' => 'trx_accordion_item'),	// Use only|except attributes to limit child shortcodes (separate multiple values with comma)
			"params" => array(
				array(
					"param_name" => "counter",
					"heading" => esc_html__("Counter", 'pabloguadi'),
					"description" => wp_kses_data( __("Display counter before each accordion title", 'pabloguadi') ),
					"class" => "",
					"value" => array("Add item numbers before each element" => "on" ),
					"type" => "checkbox"
				),
				array(
					"param_name" => "initial",
					"heading" => esc_html__("Initially opened item", 'pabloguadi'),
					"description" => wp_kses_data( __("Number of initially opened item", 'pabloguadi') ),
					"class" => "",
					"value" => 1,
					"type" => "textfield"
				),
				array(
					"param_name" => "icon_closed",
					"heading" => esc_html__("Icon while closed", 'pabloguadi'),
					"description" => wp_kses_data( __("Select icon for the closed accordion item from Fontello icons set", 'pabloguadi') ),
					"class" => "",
					"value" => pabloguadi_get_sc_param('icons'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "icon_opened",
					"heading" => esc_html__("Icon while opened", 'pabloguadi'),
					"description" => wp_kses_data( __("Select icon for the opened accordion item from Fontello icons set", 'pabloguadi') ),
					"class" => "",
					"value" => pabloguadi_get_sc_param('icons'),
					"type" => "dropdown"
				),
				pabloguadi_get_vc_param('id'),
				pabloguadi_get_vc_param('class'),
				pabloguadi_get_vc_param('animation'),
				pabloguadi_get_vc_param('css'),
				pabloguadi_get_vc_param('margin_top'),
				pabloguadi_get_vc_param('margin_bottom'),
				pabloguadi_get_vc_param('margin_left'),
				pabloguadi_get_vc_param('margin_right')
			),
			'default_content' => '
				[trx_accordion_item title="' . esc_html__( 'Item 1 title', 'pabloguadi' ) . '"][/trx_accordion_item]
				[trx_accordion_item title="' . esc_html__( 'Item 2 title', 'pabloguadi' ) . '"][/trx_accordion_item]
			',
			"custom_markup" => '
				<div class="wpb_accordion_holder wpb_holder clearfix vc_container_for_children">
					%content%
				</div>
				<div class="tab_controls">
					<button class="add_tab" title="'.esc_attr__("Add item", 'pabloguadi').'">'.esc_html__("Add item", 'pabloguadi').'</button>
				</div>
			',
			'js_view' => 'VcTrxAccordionView'
		) );
		
		
		vc_map( array(
			"base" => "trx_accordion_item",
			"name" => esc_html__("Accordion item", 'pabloguadi'),
			"description" => wp_kses_data( __("Inner accordion item", 'pabloguadi') ),
			"show_settings_on_create" => true,
			"content_element" => true,
			"is_container" => true,
			'icon' => 'icon_trx_accordion_item',
			"as_child" => array('only' => 'trx_accordion'), 	// Use only|except attributes to limit parent (separate multiple values with comma)
			"as_parent" => array('except' => 'trx_accordion'),
			"params" => array(
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'pabloguadi'),
					"description" => wp_kses_data( __("Title for current accordion item", 'pabloguadi') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "icon_closed",
					"heading" => esc_html__("Icon while closed", 'pabloguadi'),
					"description" => wp_kses_data( __("Select icon for the closed accordion item from Fontello icons set", 'pabloguadi') ),
					"class" => "",
					"value" => pabloguadi_get_sc_param('icons'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "icon_opened",
					"heading" => esc_html__("Icon while opened", 'pabloguadi'),
					"description" => wp_kses_data( __("Select icon for the opened accordion item from Fontello icons set", 'pabloguadi') ),
					"class" => "",
					"value" => pabloguadi_get_sc_param('icons'),
					"type" => "dropdown"
				),
				pabloguadi_get_vc_param('id'),
				pabloguadi_get_vc_param('class'),
				pabloguadi_get_vc_param('css')
			),
		  'js_view' => 'VcTrxAccordionTabView'
		) );

		class WPBakeryShortCode_Trx_Accordion extends PABLOGUADI_VC_ShortCodeAccordion {}
		class WPBakeryShortCode_Trx_Accordion_Item extends PABLOGUADI_VC_ShortCodeAccordionItem {}
	}
}
?>