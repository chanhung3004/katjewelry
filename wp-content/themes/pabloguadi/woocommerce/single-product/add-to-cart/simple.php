<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $product;
$attributes = $product->get_attributes();
$cartOptions = [];
foreach ($attributes as $attrKey => $attrVal) {
    $acceptedKeys = ['size', 'plated', 'color'];
    if (in_array($attrKey, $acceptedKeys)) {
        $options = explode('|', $attrVal['value']);
        $cartOptions[] = ['options' => $options, 'name' => $attrKey, 'class' => 'woocommerce-ordering force-right', 'show_option_none' => __('Choose an ' . $attrVal['name'], 'woocommerce')];
    }

}

if (!$product->is_purchasable()) {
    return;
}

// Availability
$availability = $product->get_availability();
$availability_html = empty($availability['availability']) ? '' : '<p class="stock ' . esc_attr($availability['class']) . '">' . esc_html($availability['availability']) . '</p>';

echo apply_filters('woocommerce_stock_html', $availability_html, $availability['availability'], $product);
?>

<?php if ($product->is_in_stock()) : ?>

    <?php do_action('woocommerce_before_add_to_cart_form'); ?>

    <form class="cart" method="post" enctype='multipart/form-data'>
        <?php do_action('woocommerce_before_add_to_cart_button'); ?>

        <?php
        if (!$product->is_sold_individually()) {
            echo "<div class='row' style='margin-bottom: 1% '>";
            foreach ($cartOptions as $cartOption) {
                wc_dropdown_variation_attribute_options($cartOption);
            }
            echo "</div>";
            woocommerce_quantity_input(array(
                'min_value' => apply_filters('woocommerce_quantity_input_min', 1, $product),
                'max_value' => apply_filters('woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product),
                'input_value' => (isset($_POST['quantity']) ? wc_stock_amount($_POST['quantity']) : 1)
            ));

        }
        ?>
        <div class="row" style="margin-bottom: 1%">
            <input type="hidden" name="add-to-cart" value="<?php echo esc_attr($product->id); ?>"/>
        </div>
        <div class="row" style="margin-bottom: 1%">
            <button type="submit"
                    class="single_add_to_cart_button button alt"><?php echo esc_html($product->single_add_to_cart_text()); ?></button>
        </div>

        <?php do_action('woocommerce_after_add_to_cart_button'); ?>
    </form>


    <?php do_action('woocommerce_after_add_to_cart_form'); ?>

<?php endif; ?>

