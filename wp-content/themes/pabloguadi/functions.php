<?php
/**
 * Theme sprecific functions and definitions
 */

/* Theme setup section
------------------------------------------------------------------- */

// Set the content width based on the theme's design and stylesheet.
if (!isset($content_width)) $content_width = 1170; /* pixels */

// Add theme specific actions and filters
// Attention! Function were add theme specific actions and filters handlers must have priority 1
if (!function_exists('pabloguadi_theme_setup')) {
    add_action('pabloguadi_action_before_init_theme', 'pabloguadi_theme_setup', 1);
    function pabloguadi_theme_setup()
    {

        // Register theme menus
        add_filter('pabloguadi_filter_add_theme_menus', 'pabloguadi_add_theme_menus');

        // Register theme sidebars
        add_filter('pabloguadi_filter_add_theme_sidebars', 'pabloguadi_add_theme_sidebars');

        // Set options for importer
        add_filter('pabloguadi_filter_importer_options', 'pabloguadi_set_importer_options');

        // Add theme required plugins
        add_filter('pabloguadi_filter_required_plugins', 'pabloguadi_add_required_plugins');

        // Add preloader styles
        add_filter('pabloguadi_filter_add_styles_inline', 'pabloguadi_head_add_page_preloader_styles');

        // Init theme after WP is created
        add_action('wp', 'pabloguadi_core_init_theme');

        // Add theme specified classes into the body
        add_filter('body_class', 'pabloguadi_body_classes');

        // Add data to the head and to the beginning of the body
        add_action('wp_head', 'pabloguadi_head_add_page_meta', 1);
        add_action('before', 'pabloguadi_body_add_toc');
        add_action('before', 'pabloguadi_body_add_page_preloader');

        // Add data to the footer (priority 1, because priority 2 used for localize scripts)
        add_action('wp_footer', 'pabloguadi_footer_add_views_counter', 1);
        add_action('wp_footer', 'pabloguadi_footer_add_theme_customizer', 1);
        add_action('wp_footer', 'pabloguadi_footer_add_scroll_to_top', 1);

        // Set list of the theme required plugins
        pabloguadi_storage_set('required_plugins', array(
                'essgrids',
                'instagram_feed',
                'revslider',
                'trx_utils',
                'visual_composer',
                'woocommerce',
                'mailchimp',
            )
        );

        // Set list of the theme required custom fonts from folder /css/font-faces
        // Attention! Font's folder must have name equal to the font's name
        pabloguadi_storage_set('required_custom_fonts', array(
                'Amadeus'
            )
        );

        pabloguadi_storage_set('demo_data_url', esc_url(pabloguadi_get_protocol() . '://pabloguadi.ancorathemes.com/demo/'));

    }
}


// Add/Remove theme nav menus
if (!function_exists('pabloguadi_add_theme_menus')) {
    //add_filter( 'pabloguadi_filter_add_theme_menus', 'pabloguadi_add_theme_menus' );
    function pabloguadi_add_theme_menus($menus)
    {
        return $menus;
    }
}


// Add theme specific widgetized areas
if (!function_exists('pabloguadi_add_theme_sidebars')) {
    //add_filter( 'pabloguadi_filter_add_theme_sidebars',	'pabloguadi_add_theme_sidebars' );
    function pabloguadi_add_theme_sidebars($sidebars = array())
    {
        return[];
//        if (is_array($sidebars)) {
//            $theme_sidebars = array(
//                'sidebar_main' => esc_html__('Main Sidebar', 'pabloguadi'),
//                'sidebar_footer' => esc_html__('Footer Sidebar', 'pabloguadi')
//            );
//            if (function_exists('pabloguadi_exists_woocommerce') && pabloguadi_exists_woocommerce()) {
//                $theme_sidebars['sidebar_cart'] = esc_html__('WooCommerce Cart Sidebar', 'pabloguadi');
//            }
//            $sidebars = array_merge($theme_sidebars, $sidebars);
//        }
//        return $sidebars;
    }
}


// Add theme required plugins
if (!function_exists('pabloguadi_add_required_plugins')) {
    //add_filter( 'pabloguadi_filter_required_plugins',		'pabloguadi_add_required_plugins' );
    function pabloguadi_add_required_plugins($plugins)
    {
        $plugins[] = array(
            'name' => esc_html__('PabloGuadi Utilities', 'pabloguadi'),
            'version' => '2.8',                    // Minimal required version
            'slug' => 'trx_utils',
            'source' => pabloguadi_get_file_dir('plugins/install/trx_utils.zip'),
            'required' => true
        );
        return $plugins;
    }
}


// One-click import support
//------------------------------------------------------------------------

// Set theme specific importer options
if (!function_exists('pabloguadi_set_importer_options')) {
    //add_filter( 'pabloguadi_filter_importer_options',	'pabloguadi_set_importer_options' );
    function pabloguadi_set_importer_options($options = array())
    {
        if (is_array($options)) {
            // Default demo
            $options['demo_url'] = pabloguadi_storage_get('demo_data_url');
            // Default demo
            $options['files']['default']['title'] = esc_html__('Default Demo', 'pabloguadi');
            $options['files']['default']['domain_dev'] = esc_url(pabloguadi_get_protocol() . '://pabloguadi.dv.ancorathemes.com');        // Developers domain
            $options['files']['default']['domain_demo'] = esc_url(pabloguadi_get_protocol() . '://pabloguadi.ancorathemes.com');        // Demo-site domain
            // If theme need more demo - just copy 'default' and change required parameter
            // For example:
            // 		$options['files']['dark_demo'] = $options['files']['default'];
            // 		$options['files']['dark_demo']['title'] = esc_html__('Dark Demo', 'pabloguadi');
        }
        return $options;
    }
}


// Add data to the head and to the beginning of the body
//------------------------------------------------------------------------

// Add theme specified classes to the body tag
if (!function_exists('pabloguadi_body_classes')) {
    //add_filter( 'body_class', 'pabloguadi_body_classes' );
    function pabloguadi_body_classes($classes)
    {

        $classes[] = 'pabloguadi_body';
        $classes[] = 'body_style_' . trim(pabloguadi_get_custom_option('body_style'));
        $classes[] = 'body_' . (pabloguadi_get_custom_option('body_filled') == 'yes' ? 'filled' : 'transparent');
        $classes[] = 'article_style_' . trim(pabloguadi_get_custom_option('article_style'));

        $blog_style = pabloguadi_get_custom_option(is_singular() && !pabloguadi_storage_get('blog_streampage') ? 'single_style' : 'blog_style');
        $classes[] = 'layout_' . trim($blog_style);
        $classes[] = 'template_' . trim(pabloguadi_get_template_name($blog_style));

        $body_scheme = pabloguadi_get_custom_option('body_scheme');
        if (empty($body_scheme) || pabloguadi_is_inherit_option($body_scheme)) $body_scheme = 'original';
        $classes[] = 'scheme_' . $body_scheme;

        $top_panel_position = pabloguadi_get_custom_option('top_panel_position');
        if (!pabloguadi_param_is_off($top_panel_position)) {
            $classes[] = 'top_panel_show';
            $classes[] = 'top_panel_' . trim($top_panel_position);
        } else
            $classes[] = 'top_panel_hide';
        $classes[] = pabloguadi_get_sidebar_class();

        if (pabloguadi_get_custom_option('show_video_bg') == 'yes' && (pabloguadi_get_custom_option('video_bg_youtube_code') != '' || pabloguadi_get_custom_option('video_bg_url') != ''))
            $classes[] = 'video_bg_show';

        if (!pabloguadi_param_is_off(pabloguadi_get_theme_option('page_preloader')))
            $classes[] = 'preloader';

        return $classes;
    }
}


// Add page meta to the head
if (!function_exists('pabloguadi_head_add_page_meta')) {
    //add_action('wp_head', 'pabloguadi_head_add_page_meta', 1);
    function pabloguadi_head_add_page_meta()
    {
        ?>
        <meta charset="<?php bloginfo('charset'); ?>"/>
        <meta name="viewport"
              content="width=device-width, initial-scale=1<?php if (pabloguadi_get_theme_option('responsive_layouts') == 'yes') echo ', maximum-scale=1'; ?>">
        <meta name="format-detection" content="telephone=no">

        <link rel="profile" href="http://gmpg.org/xfn/11"/>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
        <?php
    }
}

// Add page preloader styles to the head
if (!function_exists('pabloguadi_head_add_page_preloader_styles')) {
    //add_filter('pabloguadi_filter_add_styles_inline', 'pabloguadi_head_add_page_preloader_styles');
    function pabloguadi_head_add_page_preloader_styles($css)
    {
        if (($preloader = pabloguadi_get_theme_option('page_preloader')) != 'none') {
            $image = pabloguadi_get_theme_option('page_preloader_image');
            $bg_clr = pabloguadi_get_scheme_color('bg_color');
            $link_clr = pabloguadi_get_scheme_color('text_link');
            $css .= '
				#page_preloader {
					background-color: ' . esc_attr($bg_clr) . ';'
                . ($preloader == 'custom' && $image
                    ? 'background-image:url(' . esc_url($image) . ');'
                    : ''
                )
                . '
				}
				.preloader_wrap > div {
					background-color: ' . esc_attr($link_clr) . ';
				}';
        }
        return $css;
    }
}

// Add TOC anchors to the beginning of the body
if (!function_exists('pabloguadi_body_add_toc')) {
    //add_action('before', 'pabloguadi_body_add_toc');
    function pabloguadi_body_add_toc()
    {
        // Add TOC items 'Home' and "To top"
        if (pabloguadi_get_custom_option('menu_toc_home') == 'yes')
            pabloguadi_show_layout(pabloguadi_sc_anchor(array(
                    'id' => "toc_home",
                    'title' => esc_html__('Home', 'pabloguadi'),
                    'description' => esc_html__('{{Return to Home}} - ||navigate to home page of the site', 'pabloguadi'),
                    'icon' => "icon-home",
                    'separator' => "yes",
                    'url' => esc_url(home_url('/'))
                )
            ));
        if (pabloguadi_get_custom_option('menu_toc_top') == 'yes')
            pabloguadi_show_layout(pabloguadi_sc_anchor(array(
                    'id' => "toc_top",
                    'title' => esc_html__('To Top', 'pabloguadi'),
                    'description' => esc_html__('{{Back to top}} - ||scroll to top of the page', 'pabloguadi'),
                    'icon' => "icon-double-up",
                    'separator' => "yes")
            ));
    }
}

// Add page preloader to the beginning of the body
if (!function_exists('pabloguadi_body_add_page_preloader')) {
    //add_action('before', 'pabloguadi_body_add_page_preloader');
    function pabloguadi_body_add_page_preloader()
    {
        if (($preloader = pabloguadi_get_theme_option('page_preloader')) != 'none' && ($preloader != 'custom' || ($image = pabloguadi_get_theme_option('page_preloader_image')) != '')) {
            ?>
            <div id="page_preloader"><?php
            if ($preloader == 'circle') {
                ?>
            <div class="preloader_wrap preloader_<?php echo esc_attr($preloader); ?>">
                    <div class="preloader_circ1"></div>
                    <div class="preloader_circ2"></div>
                    <div class="preloader_circ3"></div>
                    <div class="preloader_circ4"></div></div><?php
            } else if ($preloader == 'square') {
                ?>
            <div class="preloader_wrap preloader_<?php echo esc_attr($preloader); ?>">
                    <div class="preloader_square1"></div>
                    <div class="preloader_square2"></div></div><?php
            }
            ?></div><?php
        }
    }
}


// Add data to the footer
//------------------------------------------------------------------------

// Add post/page views counter
if (!function_exists('pabloguadi_footer_add_views_counter')) {
    //add_action('wp_footer', 'pabloguadi_footer_add_views_counter');
    function pabloguadi_footer_add_views_counter()
    {
        // Post/Page views counter
        get_template_part(pabloguadi_get_file_slug('templates/_parts/views-counter.php'));
    }
}

// Add theme customizer
if (!function_exists('pabloguadi_footer_add_theme_customizer')) {
    //add_action('wp_footer', 'pabloguadi_footer_add_theme_customizer');
    function pabloguadi_footer_add_theme_customizer()
    {
        // Front customizer
        if (pabloguadi_get_custom_option('show_theme_customizer') == 'yes') {
            require_once PABLOGUADI_FW_PATH . 'core/core.customizer/front.customizer.php';
        }
    }
}

// Add scroll to top button
if (!function_exists('pabloguadi_footer_add_scroll_to_top')) {
    //add_action('wp_footer', 'pabloguadi_footer_add_scroll_to_top');
    function pabloguadi_footer_add_scroll_to_top()
    {
        ?><a href="#" class="scroll_to_top icon-up"
             title="<?php esc_attr_e('Scroll to top', 'pabloguadi'); ?>"></a><?php
    }
}

function pabloguadi_move_comment_field_to_bottom($fields)
{
    $comment_field = $fields['comment'];
    unset($fields['comment']);
    $fields['comment'] = $comment_field;
    return $fields;
}

add_filter('comment_form_fields', 'pabloguadi_move_comment_field_to_bottom');
/**
 * Theme customize by HUNG VO
 */
/**
 * PDP customize
 */
add_filter('woocommerce_product_tabs', 'woo_customize_product_tabs', 98);

function woo_customize_product_tabs($tabs)
{

    unset($tabs['description']);        // Remove the description tab
    unset($tabs['reviews']);            // Remove the reviews tab
    unset($tabs['additional_information']);    // Remove the additional information tab
    $tabs['size'] = array(
        'title' => __('Size Guide', 'woocommerce'),
        'priority' => 50,
        'callback' => 'kat_size_tab_content'
    );
    $tabs['policies'] = array(
        'title' => __('Shipping Policies', 'woocommerce'),
        'priority' => 51,
        'callback' => 'kat_policies_tab_content'
    );
    return $tabs;

}

function kat_size_tab_content()
{

    // The new tab content

    echo '<h2>Size </h2>';
    echo '<p>Input your customize here buddies</p>';

}

function kat_policies_tab_content()
{

    // The new tab content

    echo '<h2>New Product Tab</h2>';
    echo '<p>Input your customize here buddies</p>';

}

//add_action( 'widgets_init', 'remove_some_widgets', 11 );

add_filter('woocommerce_add_cart_item_data', 'wdm_add_item_data', 10, 2);
if (!function_exists('wdm_add_item_data')) {
    function wdm_add_item_data($cart_item_data, $product_id)
    {
        /*Here, We are adding item in WooCommerce session with, wdm_user_custom_data_value name*/

        global $woocommerce;

//		echo '<pre>';
//		var_dump($cart_item_data);
//		echo '</pre>';
//		die('123asd');
        $option = [];
        if (isset($_POST['size'])) {
            $option['size'] = $_POST['size'];
        }
        if (isset($_POST['plated'])) {
            $option['plated'] = $_POST['plated'];
        }
        if (isset($_POST['color'])) {
            $option['color'] = $_POST['color'];
        }
        if (empty($option))
            return $cart_item_data;
        else {
            if (empty($cart_item_data))

                return $option;
            else
                return array_merge($cart_item_data, $option);
        }
        //Unset our custom session variable, as it is no longer needed.
    }
}

add_filter('woocommerce_get_cart_item_from_session', 'wdm_get_cart_items_from_session', 1, 3);
if (!function_exists('wdm_get_cart_items_from_session')) {
    function wdm_get_cart_items_from_session($item, $values, $key)
    {

        if (array_key_exists('size', $values)) {
                $item['size'] = $values['size'];
        }
        if (array_key_exists('plated', $values)) {
            $item['plated'] = $values['plated'];
        }
        if (array_key_exists('color', $values)) {
            $item['color'] = $values['color'];
        }
        return $item;
    }
}

function render_meta_on_cart_item($title = null, $cart_item = null, $cart_item_key = null)
{
    $title .= '<dl style="padding-left: 5px;">';
    if ($cart_item_key && is_cart() && isset($cart_item['size']) && trim($cart_item['size']) !== '') {
        $title .= '<p> Size: ' . $cart_item['size'] . '</p> ';
    }
    if ($cart_item_key && is_cart() && isset($cart_item['plated']) && trim($cart_item['plated']) !== '') {

        $title .= '<p> Plate: ' . $cart_item['plated'] . '</p> ';
    }
    if ($cart_item_key && is_cart() && isset($cart_item['color']) && trim($cart_item['color']) !== '') {

        $title .= '<p> Color: ' . $cart_item['color'] . '</p> ';
    }
    $title .= '</dl>';
    echo $title;
}

add_filter('woocommerce_cart_item_name', 'render_meta_on_cart_item', 1, 3);

function kat_order_meta_handler($item_id, $cart_item, $cart_item_key)
{

    if (isset($cart_item['size']) && trim($cart_item['size']) !== '') {
        wc_add_order_item_meta($item_id, "size", $cart_item['size']);
    }
    if (isset($cart_item['plated']) && trim($cart_item['plated']) !== '') {
        wc_add_order_item_meta($item_id, "plated",$cart_item['plated']);
    }
    if (isset($cart_item['color']) && trim($cart_item['color']) !== '') {
        wc_add_order_item_meta($item_id, "color", $cart_item['color']);
    }
}

add_action('woocommerce_add_order_item_meta', 'kat_order_meta_handler', 1, 3);
add_action('woocommerce_before_cart_item_quantity_zero', 'wdm_remove_user_custom_data_options_from_cart', 1, 1);
if (!function_exists('wdm_remove_user_custom_data_options_from_cart')) {
    function wdm_remove_user_custom_data_options_from_cart($cart_item_key)
    {
        global $woocommerce;
        // Get cart
        $cart = $woocommerce->cart->get_cart();
        // For each item in cart, if item is upsell of deleted product, delete it
        foreach ($cart as $key => $values) {
            if ($values['size'] == $cart_item_key)
                unset($woocommerce->cart->cart_contents[$key]);
            if ($values['plated'] == $cart_item_key)
                unset($woocommerce->cart->cart_contents[$key]);
            if ($values['color'] == $cart_item_key)
                unset($woocommerce->cart->cart_contents[$key]);
        }
    }
}
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );

add_action( 'woocommerce_single_product_summary', 'my_woocommerce_template_single_rating', 10 );

function my_woocommerce_template_single_rating() {
    global $product;

    if ( $product->post->comment_status === 'open' )
        wc_get_template( 'single-product/rating.php' );

    return true;
}
add_filter ( 'woocommerce_product_thumbnails_columns', 'xx_thumb_cols' );
function xx_thumb_cols() {
    return 4; // .last class applied to every 4th thumbnail
}

/**
 * END OF Theme customize
 */

//-------------------------------------------------------------------
require_once trailingslashit(get_template_directory()) . 'fw/loader.php';
?>