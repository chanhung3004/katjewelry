<?php
/* Instagram Feed support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('pabloguadi_instagram_feed_theme_setup')) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_instagram_feed_theme_setup', 1 );
	function pabloguadi_instagram_feed_theme_setup() {
		if (is_admin()) {
			add_filter( 'pabloguadi_filter_required_plugins',					'pabloguadi_instagram_feed_required_plugins' );
		}
	}
}

// Check if Instagram Feed installed and activated
if ( !function_exists( 'pabloguadi_exists_instagram_feed' ) ) {
	function pabloguadi_exists_instagram_feed() {
		return defined('SBIVER');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'pabloguadi_instagram_feed_required_plugins' ) ) {
	//add_filter('pabloguadi_filter_required_plugins',	'pabloguadi_instagram_feed_required_plugins');
	function pabloguadi_instagram_feed_required_plugins($list=array()) {
		if (in_array('instagram_feed', pabloguadi_storage_get('required_plugins')))
			$list[] = array(
					'name' 		=> esc_html__('Instagram Feed', 'pabloguadi'),
					'slug' 		=> 'instagram-feed',
					'required' 	=> false
				);
		return $list;
	}
}

?>